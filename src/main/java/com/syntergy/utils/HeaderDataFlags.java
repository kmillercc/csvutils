package com.syntergy.utils;

import com.syntergy.utils.ColumnHandlers.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Kit on 3/13/2015.
 */
@SuppressWarnings("unchecked")
class HeaderDataFlags extends HashMap<String, Boolean> {

    private final List<String> cols;
    private boolean hasMetadataCols = false;

    public HeaderDataFlags(String[] header) {
        super(13);
        cols = Arrays.asList(header);

        this.put("hasRmCols", hasRmCols());
        this.put("hasPoCols", hasPoCols());

        this.put("hasPoCircCols", hasPoCircCols());
        this.put("hasPoBoxCols", hasPoBoxCols());
        this.put("hasPoLocatorCols", hasPoLocatorCols());
        this.put("hasPoXferCols", hasPoXferCols());
        this.put("hasPoLabelCols", hasPoLabelCols());


        this.put("hasEmailCols", hasEmailCols());
        this.put("hasPermCols", hasPermCols());
        this.put("hasClassCols", hasClassCols());
        this.put("hasDapiNodeCols", hasDapiNodeCols());
        this.put("hasOwnerCols", hasOwnerCols());
        this.put("hasNickName", hasNickName());
        this.put("hasNewName", hasNewName());
        this.put("hasFiles", hasFiles());
        this.put("hasAttrCols", hasAttrCols());
        this.put("hasSystemAttrCols", hasSystemAttrCols());
        this.put("hasMetadataCols", hasMetadataCols());
        this.put("hasVersionCols", hasVersionCols());
        this.put("hasContractFileCols", hasContractFileCols());

        this.put("targetPathIsParent", targetPathIsParent());
    }

    private Boolean targetPathIsParent() {
        if (cols.contains(CommonColHandler.Columns.$OBJECTNAME.toString()) && cols.contains(CommonColHandler.Columns.$TARGETPATH.toString())){
            return true;
        }

        return false;
    }

    private Boolean hasRmCols() {
        if (Utils.arrayHasColumnType(RmColHandler.class, cols)){
            hasMetadataCols = true;
            return true;
        }
        return false;
    }

    private Boolean hasPoCols() {
        if (Utils.arrayHasColumnType(PoColHandler.class, cols)){
            hasMetadataCols = true;
            return true;
        }
        return false;
    }

    private Boolean hasPoCircCols() {
        if (Utils.arrayHasColumnType(PoCircColHandler.class, cols)){
            hasMetadataCols = true;
            return true;
        }
        return false;
    }

    private Boolean hasPoBoxCols() {
        if (Utils.arrayHasColumnType(PoBoxColHandler.class, cols)){
            hasMetadataCols = true;
            return true;
        }
        return false;
    }

    private Boolean hasPoXferCols() {
        if (Utils.arrayHasColumnType(PoTransferColHandler.class, cols)){
            hasMetadataCols = true;
            return true;
        }
        return false;
    }

    private Boolean hasPoLocatorCols() {
        if (Utils.arrayHasColumnType(PoLocatorColHandler.class, cols)){
            hasMetadataCols = true;
            return true;
        }
        return false;
    }

    private Boolean hasPoLabelCols() {
        if (Utils.arrayHasColumnType(PoLabelColHandler.class, cols)){
            hasMetadataCols = true;
            return true;
        }
        return false;
    }

    private Boolean hasVersionCols(){
        return Utils.arrayHasColumnType(VersionColHandler.class, cols);
    }

    private Boolean hasEmailCols() {
        if (Utils.arrayHasColumnType(EmailColHandler.class, cols)){
            hasMetadataCols = true;
            return true;
        }
        return false;
    }

    private Boolean hasPermCols() {
        return Utils.arrayHasColumnType(PermColHandler.class, cols);
    }

    private Boolean hasClassCols() {
        if (cols.contains(ClassColHandler.Columns.$CLASSIFICATION.toString())){
            hasMetadataCols = true;
            return true;
        }
        return false;
    }

    private Boolean hasDapiNodeCols() {
        if (Utils.arrayHasColumnType(DapiColHandler.class, cols)){
            hasMetadataCols = true;
            return true;
        }
        return false;
    }

    private Boolean hasOwnerCols() {
        if (Utils.arrayHasColumnType(OwnerColHandler.class, cols)){
            hasMetadataCols = true;
            return true;
        }
        return false;
    }

    private Boolean hasContractFileCols() {
        if (Utils.arrayHasColumnType(ContractFileColHandler.class, cols)){
            hasMetadataCols = true;
            return true;
        }
        return false;
    }

    private Boolean hasNickName() {
        if (cols.contains(CommonColHandler.Columns.$NICKNAME.toString())){
            hasMetadataCols = true;
            return true;
        }
        return false;
    }

    private Boolean hasNewName() {
        return cols.contains(CommonColHandler.Columns.$NEWNAME.toString());
    }

    private Boolean hasFiles() {
         return cols.contains(CommonColHandler.Columns.$SOURCEPATH.toString()) || cols.contains(CommonColHandler.Columns.$FILENAME.toString());
    }

    private Boolean hasAttrCols() {
        // loop through remaining columns and see if we have any categories
        // its good enough that we check only the format of the column -- i.e. no leading $ and the presence of colons in the name
        for (String col: cols){
            if (Utils.isCatAttrCol(col)){
                hasMetadataCols = true;
                return true;
            }
        }
        return false;
    }

    private Boolean hasSystemAttrCols() {
        // loop through remaining columns and see if we have any system attributes
        for (String col: cols){
            if (Utils.isSysAttrCol(col)){
                hasMetadataCols = true;
                return true;
            }
        }
        return false;
    }

    private Boolean hasMetadataCols() {
        return hasMetadataCols;
    }

}
