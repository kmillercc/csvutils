package com.syntergy.utils;

/**
 * Created by Kit on 3/19/2015.
 */
public enum Action {
    CREATEUSERGROUP,
    UPDATEUSERGROUP,
    DELETEUSERGROUP,
    CREATE,
    ADDVERSION,
    MOVE,
    COPY,
    PURGE,
    DELETE,
    UPDATE,
    PERMS,
    RENAME
}
