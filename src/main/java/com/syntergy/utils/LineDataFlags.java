package com.syntergy.utils;

import com.syntergy.utils.ColumnHandlers.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Kit on 3/13/2015.
 */
@SuppressWarnings("unchecked")
class LineDataFlags extends HashMap<String, Boolean> {

    private final Map<String, Object> csvData;
    private boolean hasMetadata = false;

    public LineDataFlags(Map<String, Object> csvData,
                         Map<String, Boolean> headerDataFlags) {

        /* the header flags
            "hasRmCols"
            "hasPoCols"
            "hasEmailCols"
            "hasPermCols"
            "hasClassCols"
            "hasDapiNodeCols"
            "hasOwnerCols"
            "hasNickName"
            "hasNewName"
            "hasFiles"
            "hasAttrCols"
            "hasSystemAttrCols"
            "hasMetadataCols"
	*/

        super(13);
        this.csvData = csvData;
        this.put("hasRecManData", headerDataFlags.get("hasRmCols") && hasRecManData());

        this.put("hasPhysObjData", headerDataFlags.get("hasPoCols") && hasPhysObjData());
        this.put("hasPhysObjCirc", headerDataFlags.get("hasPoCircCols") && hasPoCircData());
        this.put("hasPhysObjBox", headerDataFlags.get("hasPoBoxCols") && hasPoBoxData());
        this.put("hasPhysObjLocator", headerDataFlags.get("hasPoLocatorCols") && hasPoLocatorData());
        this.put("hasPhysObjXfer", headerDataFlags.get("hasPoXferCols") && hasPoXferData());
        this.put("hasPhysObjLabel", headerDataFlags.get("hasPoLabelCols") && hasPoLabelData());

        this.put("hasEmailData", headerDataFlags.get("hasEmailCols") && hasEmailData());
        this.put("hasPermData", headerDataFlags.get("hasPermCols") && hasPermData());
        this.put("hasClassification", headerDataFlags.get("hasClassCols") && hasClassification());
        this.put("hasNickName", headerDataFlags.get("hasNickName") && hasNickName());
        this.put("hasDapiNodeData", headerDataFlags.get("hasDapiNodeCols") && hasDapiNodeData());
        this.put("hasOwnerData", headerDataFlags.get("hasOwnerCols") && hasOwnerData());
        this.put("hasNewName", headerDataFlags.get("hasNewName") &&  hasNewName());
        this.put("hasFiles", headerDataFlags.get("hasFiles") && hasFiles());
        this.put("hasAttrData", headerDataFlags.get("hasAttrCols") && hasAttrData());
        this.put("hasSystemAttrs", headerDataFlags.get("hasSystemAttrCols") && hasSystemAttrs());
        this.put("hasMetadata", headerDataFlags.get("hasMetadataCols") && hasMetadata());
        this.put("hasVersionData", headerDataFlags.get("hasVersionCols") && hasVersionData());

        this.put("hasContractFileData", headerDataFlags.get("hasContractFileCols") && hasContractFileData());
    }

    private Boolean hasRecManData() {
        if (Utils.mapHasDataType(RmColHandler.class, csvData, true)){
            this.hasMetadata = true;
            return true;
        }
        return false;
    }

    private Boolean hasPhysObjData() {
        if (Utils.mapHasDataType(PoColHandler.class, csvData, true)){
            this.hasMetadata = true;
            return true;
        }
        return false;
    }

    private Boolean hasPoCircData() {
        if (Utils.mapHasDataType(PoCircColHandler.class, csvData, true)){
            this.hasMetadata = true;
            return true;
        }
        return false;
    }

    private Boolean hasPoBoxData() {
        return Utils.mapHasDataType(PoBoxColHandler.class, csvData, true);
    }

    private Boolean hasPoLocatorData() {
        return Utils.mapHasDataType(PoLocatorColHandler.class, csvData, true);
    }

    private Boolean hasPoXferData() {
        return Utils.mapHasDataType(PoTransferColHandler.class, csvData, true);
    }

    private Boolean hasPoLabelData() {
        return Utils.mapHasDataType(PoLabelColHandler.class, csvData, true);
    }

    private Boolean hasVersionData() {
        return Utils.mapHasDataType(VersionColHandler.class, csvData, true);
    }

    private Boolean hasEmailData() {
        if (Utils.mapHasDataType(EmailColHandler.class, csvData, true)){
            this.hasMetadata = true;
            return true;
        }
        return false;
    }

    private Boolean hasNickName() {
        if (Utils.mapHasColumn(CommonColHandler.Columns.$NICKNAME, csvData, true)){
            this.hasMetadata = true;
            return true;
        }
        return false;
    }

    private Boolean hasDapiNodeData() {
        if (Utils.mapHasDataType(DapiColHandler.class, csvData, true)){
            this.hasMetadata = true;
            return true;
        }
        return false;
    }

    private Boolean hasContractFileData() {
        if (Utils.mapHasDataType(ContractFileColHandler.class, csvData, true)) {
            this.hasMetadata = true;
            return true;
        }
        return false;
    }

    private Boolean hasPermData() {
        return Utils.mapHasDataType(PermColHandler.class, csvData, true);
    }

    private Boolean hasClassification() {
        if (Utils.mapHasColumn(ClassColHandler.Columns.$CLASSIFICATION, csvData, true)){
            this.hasMetadata = true;
            return true;
        }
        return false;
    }

    private Boolean hasOwnerData() {

        if (Utils.mapHasDataType(OwnerColHandler.class, csvData, true)){
            this.hasMetadata = true;
            return true;
        }
        return false;
    }

    private Boolean hasNewName() {
        return Utils.mapHasColumn(CommonColHandler.Columns.$NEWNAME, csvData, true);
    }

    private Boolean hasFiles() {
        return Utils.mapHasColumn(CommonColHandler.Columns.$SOURCEPATH, csvData, true) || Utils.mapHasColumn(CommonColHandler.Columns.$FILENAME, csvData, true);
    }

    private Boolean hasAttrData() {
        // loop through remaining columns and see if we have any categories
        // its good enough that we check only the format of the column -- i.e. no leading $ and the presence of colons in the name
        for (String col: csvData.keySet()){
            if (csvData.get(col) != null && Utils.isCatAttrCol(col)){
                this.hasMetadata = true;
                return true;
            }
        }
        return false;
    }

    private Boolean hasSystemAttrs() {
        // loop through remaining columns and see if we have any system attributes
        for (String col: csvData.keySet()){
            if (csvData.get(col) != null && Utils.isSysAttrCol(col)){
                this.hasMetadata = true;
                return true;
            }
        }
        return false;
    }

    private Boolean hasMetadata() {
        return this.hasMetadata;
    }

}
