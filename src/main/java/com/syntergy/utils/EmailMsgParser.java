package com.syntergy.utils;

import com.auxilii.msgparser.Message;
import com.auxilii.msgparser.MsgParser;
import com.auxilii.msgparser.RecipientEntry;

import java.io.IOException;
import java.util.*;

/**
 * com.syntergy.utils
 * CsvUtils
 * Created by Kit on 6/9/2015.
 */
public class EmailMsgParser {

    public static String GetMsgBody(String msgPath) throws ReturnedException {
        String body;

        try {

            MsgParser msgp = new MsgParser();
            Message msg = msgp.parseMsg(msgPath);

            body = msg.getBodyText();

        } catch (IOException e) {
            throw new ReturnedException(e);
        }
        return body;
    }

    public static List<Map<String, Object>> GetMsgParticipants(String msgPath) throws ReturnedException {

        List<Map<String, Object>> participants = new ArrayList<Map<String, Object>>();
        MsgParser msgp = new MsgParser();
        Message msg;
        try {
            msg = msgp.parseMsg(msgPath);
        } catch (IOException e) {
            throw new ReturnedException(e);
        }

        // put the sender in
        Map<String, Object> p = new HashMap<String, Object>();
        p.put("EmailAddress", msg.getFromEmail());
        p.put("FullName", msg.getFromName());
        p.put("ParticipantRole", 1);
        p.put("ParticipantType", "from");
        p.put("SystemProvided", 0);
        participants.add(p);

        // get the to recipients
        String bccStr = msg.getDisplayBcc();
        String ccStr = msg.getDisplayCc();
        List<RecipientEntry> recipientEntries = msg.getRecipients();
        ListIterator<RecipientEntry> it = recipientEntries.listIterator();
        RecipientEntry recipientEntry;
        while (it.hasNext()) {
            recipientEntry = it.next();
            String emailStr = recipientEntry.getToEmail();
            p = new HashMap<String, Object>();
            p.put("EmailAddress", emailStr);
            p.put("FullName", recipientEntry.getToName());
            p.put("ParticipantRole", 2);
            if (bccStr.contains(emailStr)) {
                p.put("ParticipantType", "bcc");
            }
            if (ccStr.contains(emailStr)) {
                p.put("ParticipantType", "cc");
            } else {
                p.put("ParticipantType", "to");
            }
            p.put("SystemProvided", 0);
            participants.add(p);
        }


        return participants;
    }

}