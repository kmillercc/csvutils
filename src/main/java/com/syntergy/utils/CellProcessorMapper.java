package com.syntergy.utils;

import com.opentext.livelink.oml.OScriptObject;
import com.syntergy.utils.CellProcessors.ParseOScriptInt;
import com.syntergy.utils.CellProcessors.ParseOScriptVal;
import com.syntergy.utils.ColumnHandlers.*;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.Trim;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.exception.SuperCsvException;

import java.util.List;

/**
 * Created by Kit on 3/18/2015.
 */
class CellProcessorMapper {

    static CellProcessor[] getDefaultProcessors(String[] header, ValidationLogger logger){
        int len = header.length;
        CellProcessor[] processors = new CellProcessor[len];
        for (int i = 0; i < len; i++) {
            String col = header[i];

            if (col.equals("$OBJECTID") || col.equals("$NODEID") || col.equals("NodeID")){
                processors[i] = processors[i] = new Optional(new ParseOScriptInt(CommonColHandler.Columns.$OBJECTID, logger));
            }else{
                processors[i] = new Optional(new Trim());
            }

        }
        return processors;
    }

    static CellProcessor[] getProcessors(String[] header,
                                         List<String> dateFormats,
                                         OScriptObject validateWrapper,
                                         String validateFuncName,
                                         ValidationLogger logger) throws SuperCsvException {
        int len = header.length;
        CellProcessor[] processors = new CellProcessor[len];
        for (int i = 0; i < len; i++) {
            String col = header[i];
            ColHandler c;

            if (CommonColHandler.contains(col)){
                c = new CommonColHandler(validateWrapper, validateFuncName, logger);
                processors[i] = c.getProcessor(col);
            }
            else if (DapiColHandler.contains(col)){
                c = new DapiColHandler(validateWrapper, validateFuncName, dateFormats, logger);
                processors[i] = c.getProcessor(col);
            }
            else if (ClassColHandler.contains(col)){
                c = new ClassColHandler(validateWrapper, validateFuncName, logger);
                processors[i] = c.getProcessor(col);
            }
            else if (EmailColHandler.contains(col)){
                c = new EmailColHandler(validateWrapper, validateFuncName, dateFormats, logger);
                processors[i] = c.getProcessor(col);
            }
            else if (OwnerColHandler.contains(col)){
                c = new OwnerColHandler(validateWrapper, validateFuncName, logger);
                processors[i] = c.getProcessor(col);
            }
            else if (PermColHandler.contains(col)){
                c = new PermColHandler(validateWrapper, validateFuncName, logger);
                processors[i] = c.getProcessor(col);
            }
            else if (PoColHandler.contains(col)){
                c = new PoColHandler(validateWrapper, validateFuncName, dateFormats, logger);
                processors[i] = c.getProcessor(col);
            }
            else if (PoLabelColHandler.contains(col)){
                c = new PoLabelColHandler(validateWrapper, validateFuncName, logger);
                processors[i] = c.getProcessor(col);
            }
            else if (PoLocatorColHandler.contains(col)){
                c = new PoLocatorColHandler(validateWrapper, validateFuncName, logger);
                processors[i] = c.getProcessor(col);
            }
            else if (PoTransferColHandler.contains(col)){
                c = new PoTransferColHandler(validateWrapper, validateFuncName, logger);
                processors[i] = c.getProcessor(col);
            }
            else if (PoCreateColHandler.contains(col)){
                c = new PoCreateColHandler(validateWrapper, validateFuncName, logger);
                processors[i] = c.getProcessor(col);
            }
            else if (PoBoxColHandler.contains(col)){
                c = new PoBoxColHandler(validateWrapper, validateFuncName, logger);
                processors[i] = c.getProcessor(col);
            }
            else if (PoCircColHandler.contains(col)){
                c = new PoCircColHandler(validateWrapper, validateFuncName, dateFormats, logger);
                processors[i] = c.getProcessor(col);
            }

            else if (RmColHandler.contains(col)){
                c = new RmColHandler(validateWrapper, validateFuncName, dateFormats, logger);
                processors[i] = c.getProcessor(col);
            }
            else if (UgColHandler.contains(col)){
                c = new UgColHandler(validateWrapper, validateFuncName, logger);
                processors[i] = c.getProcessor(col);
            }
            else if (VersionColHandler.contains(col)){
                c = new VersionColHandler(validateWrapper, validateFuncName, dateFormats, logger);
                processors[i] = c.getProcessor(col);
            }
            else if (RenditionColHandler.contains(col)){
                c = new RenditionColHandler(validateWrapper, validateFuncName, logger);
                processors[i] = c.getProcessor(col);
            }
            else if (ContractFileColHandler.contains(col)){
                c = new ContractFileColHandler(validateWrapper, validateFuncName, dateFormats, logger);
                processors[i] = c.getProcessor(col);
            }
            else if (Utils.isSysAttrCol(col)){
                processors[i] = new Optional(new ParseOScriptVal(validateWrapper, col, validateFuncName, logger));
            }
            else if (Utils.isCatAttrCol(col)){
                processors[i] = new Optional(new ParseOScriptVal(validateWrapper, col, validateFuncName, logger));
            }
            else{
                throw new SuperCsvException("Invalid column '" + col + "'");
            }
        }

        return processors;
    }

}
