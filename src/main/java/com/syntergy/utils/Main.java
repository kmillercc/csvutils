package com.syntergy.utils;

import com.jdotsoft.jarloader.JarClassLoader;
import com.syntergy.utils.ColumnHandlers.DapiColHandler;
import org.apache.tika.parser.txt.CharsetDetector;
import org.apache.tika.parser.txt.CharsetMatch;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        // String csvPath = "C:\\ImportExport\\Docs\\metadata2.csv";
        // testGetHeader(csvPath);
        // testParseCsvFile(csvPath);
        // testValidateParseCsvFile(csvPath);

        //ValidationLogger logger = new ValidationLogger("C:\\ImportExport\\Sherrie\\_audit");

        //logger.testSetProcessor();

        testValidateParseCsvFile("C://ImportExport/KITTESTmetadata.csv");

        //testJsonFileToCsv();

    }

    private static void testWriteStringToFile(File file, String s) throws Exception{


        JarClassLoader jarClassLoader = new JarClassLoader();

        URLClassLoader loader = new URLClassLoader(new URL[] {
                new File("C:/OPENTEXT/CS10/ojlib/util").toURI().toURL()
        });


        try{
            File jarFile = new File("C:/OPENTEXT/CS10/module/datamanager_10_5_1/ojlib/org.apache.commons.io_2.4.0");
            URLClassLoader classLoader = new URLClassLoader(new URL[] {jarFile.toURI().toURL()});
            Class classToLoad = Class.forName ("org.apache.commons.io.FileUtils", true, classLoader);
            Method method = classToLoad.getDeclaredMethod("writeStringToFile", File.class, String.class, boolean.class);
            method.invoke (null, file, s, true);


            Class<?> c1 = jarClassLoader.loadClass("org.apache.commons.io.FileUtils");

            //Class<?> c1 = loader.loadClass("com.opentext.util.ClassUtil");
            System.out.println(c1.getCanonicalName());
            Method m = c1.getMethod("writeStringToFile", File.class, String.class, boolean.class);
            m.invoke(null, file, s, true);

        } catch (IllegalAccessException e1){
            throw new Exception(e1.getClass().getName() + " " + e1.getMessage());
        } catch (NoSuchMethodException e1){
            throw new Exception(e1.getClass().getName() + " " + e1.getMessage());
        } catch (InvocationTargetException e1){
            throw new Exception(e1.getClass().getName() + " " + e1.getMessage());
        } catch (ClassNotFoundException e1){
            throw new Exception(e1.getClass().getName() + " " + e1.getMessage());
        }

    }

    private static void testGetMsgBody(){
        try{
            System.out.println(EmailMsgParser.GetMsgBody("C:\\ImportExport\\EmailFolder\\E-mail Folder\\Are you available now.MSG"));
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    private static void testGetMsgParticipants(){
        try{
           List<Map<String,Object>> participants = EmailMsgParser.GetMsgParticipants("C:\\ImportExport\\EmailFolder\\E-mail Folder\\No Go Decision - RTI Installation From and Wfsteps upgrade module.MSG");
            ListIterator<Map<String,Object>> it = participants.listIterator();
            while (it.hasNext()){
                System.out.println(it.next());
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    private static void testArrayHasColumnType(){

        String[] header = {"$Catalog","$CreateDate","$Creator","$Description","$ModifyDate","$NickName","$ObjectID","$ObjectName","$ObjectType","$TargetPath","Content Server Categories:Demo Category:Author","Content Server Categories:Demo Category:Date Submitted","Content Server Categories:Demo Category:High Priority","Content Server Categories:Demo Category:IsAttachedCat","Content Server Categories:Demo Category:Publication Info[1]:Publication Type","Content Server Categories:Demo Category:Reference Number","Content Server Categories:Demo Category:Summary","Content Server Categories:Demo Category:Year"};
        header = CsvParser.formatHeader(header, false);


        boolean b = Utils.arrayHasColumnType(DapiColHandler.class, Arrays.asList(header));

        System.out.println("has DapiNodeColumn=" + b);

    }

    private static void testJsonFileToCsv(){

        String jsonPath = "C:\\ImportExport\\Digital Library\\metadata.json";

        // *** the csvPath will be the same as json file with different extension
        String csvPath = "C:\\ImportExport\\Digital Library\\metadata2.csv";

        long startTime = System.currentTimeMillis();

        // test out the large file support
        Integer sizeLimit = 0;

        // *** call the JsonFileToCsv Method
        try {

            String firstRow = "{'$ADVVERSIONING':'','$CATALOG':'0','$CREATEDATE':'2020-03-04T16:40:03','$CREATOR':'Admin','$DESCRIPTION':'','$MODIFYDATE':'2020-03-04T16:40:03','$NICKNAME':'','$OBJECTID':'507325','$OBJECTNAME':'_AeroReports OTR Drop Box','$OBJECTTYPE':'0','$TARGETPATH':':Enterprise:Digital Library:_AeroReports OTR Drop Box','Content Server Categories:Author, Title, Date:Abstract':'','Content Server Categories:Author, Title, Date:Author\\\\\\\\u0027s Cost Center Code':'','Content Server Categories:Author, Title, Date:Author(s)':'','Content Server Categories:Author, Title, Date:Author(s) Badge':'','Content Server Categories:Author, Title, Date:Corporate Author(s)':'','Content Server Categories:Author, Title, Date:Customer':'','Content Server Categories:Author, Title, Date:Distribution Statement':'','Content Server Categories:Author, Title, Date:Document Date[1]:Day':'','Content Server Categories:Author, Title, Date:Document Date[1]:Month':'','Content Server Categories:Author, Title, Date:Document Date[1]:Year':'','Content Server Categories:Author, Title, Date:Funding Cost Center Code':'','Content Server Categories:Author, Title, Date:IsAttachedCat':'','Content Server Categories:Author, Title, Date:Keywords':'','Content Server Categories:Author, Title, Date:Program\\\\\\\\/Project':'','Content Server Categories:Author, Title, Date:Report Number':'','Content Server Categories:Author, Title, Date:Restricted':'','Content Server Categories:Author, Title, Date:Title':''}";
            CsvWriter.JsonFileToCsv(jsonPath, csvPath, firstRow, sizeLimit);

        } catch (Exception e) {
            e.printStackTrace();

        }
        long endTime = System.currentTimeMillis();
        long duration = (endTime - startTime);
        System.out.println("JsonFileToCsv = " + duration + " ms");
    }

    private static void testValidateParseCsvFile(String csvPath){
        long startTime = System.currentTimeMillis();
        String auditPath = "C:\\ImportExport\\";
        String isoDateFormat = "yyyy-MM-dd'T'HH:mm:ss";

        String[] formats = {"M/dd/yyyy", "M/dd/yyyy HH::MM:SS", isoDateFormat};

        try{
            Map<String, Object> result = CsvParser.ValidateParseCsvFile(csvPath, auditPath, Arrays.asList(formats), null, null, "ValidateSetVal", 0);
            long endTime = System.currentTimeMillis();
            long duration = (endTime - startTime);
            System.out.println("testValidateParseCsvFile = " + duration + " ms");
            System.out.println("testValidateParseCsvFile per row = " + duration/10000.0 + " ms");
            System.out.println(result);
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    private static void testParseCsvFile(String csvPath){
        long startTime = System.currentTimeMillis();
        String isoDateFormat = "yyyy-MM-dd'T'HH:mm:ss";
        String[] formats = {"M/dd/yyyy", "M/dd/yyyy HH::MM:SS", isoDateFormat};
        String auditPath = "C:\\ImportExport\\";

        try{
            Map<String,Object> result = CsvParser.ParseCsvFile(csvPath, 1, 10, null, null);
            System.out.println(result);
            long endTime = System.currentTimeMillis();
            long duration = (endTime - startTime);
            System.out.println("testParseCsvFile = " + duration + " ms");
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    private static void testGetHeader(String csvPath) {

        try {
            Map<String, Object> rtn = CsvParser.GetHeader(csvPath, true);
            System.out.println(rtn);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    private static void testGetHeaderBOM(String csvPath){

        try{

            Map<String, Object> rtn = CsvParser.GetHeader(csvPath, false);
            List colNames = (List) rtn.get("colNames");
            String s = (String) colNames.get(0);
            System.out.println();
            System.out.println("Unicode representation of the string is:");
            System.out.println(escapeUnicode(s));
            System.out.println("Without removing the BOM, the first character of the first header column is " + s.charAt(0));
            System.out.println("The second character is " + s.charAt(1));


            rtn = CsvParser.GetHeader(csvPath, true);
            colNames = (List) rtn.get("colNames");
            s = (String) colNames.get(0);
            System.out.println();
            System.out.println("With removal of BOM, the first character of the first header column is " + s.charAt(0));
            System.out.println("The second character is " + s.charAt(1));
            System.out.println();
            CharsetDetector detector = new CharsetDetector();
            detector.setText(s.getBytes());
            for (CharsetMatch match : detector.detectAll()){
                System.out.println(match.getName());
            }
            System.out.println();
            System.out.println("Unicode representation of the string is:");
            System.out.println(escapeUnicode(s));

        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    public static String escapeUnicode(String input) {
        StringBuilder b = new StringBuilder(input.length());
        Formatter f = new Formatter(b);
        for (char c : input.toCharArray()) {
            if (c < 128) {
                b.append(c);
            } else {
                f.format("\\u%04x", (int) c);
            }
        }
        return b.toString();
    }

    private static void testParseDataIdFile(){
        long startTime = System.currentTimeMillis();
        String csvPath = "C:\\ImportExport\\dataids.txt";
        String isoDateFormat = "yyyy-MM-dd'T'HH:mm:ss";
        String auditPath = "C:\\ImportExport\\";
        String[] formats = {"M/dd/yyyy", "M/dd/yyyy HH::MM:SS", isoDateFormat};

        try{
            Map<String,Object> result = CsvParser.ParseDataIdFile(csvPath, 1, 100000, null, null, auditPath);
            System.out.println(result);
            long endTime = System.currentTimeMillis();
            long duration = (endTime - startTime);
            System.out.println("testParseCsvFile = " + duration + " ms");
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

}
