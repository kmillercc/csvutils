package com.syntergy.utils;

/**
 * Created by Kit on 4/7/17.
 */
public class ReturnedException extends Exception {

    public ReturnedException(Exception e){
        super(e);
    }

    public ReturnedException(Exception e, ValidationLogger logger){
        super(e);

        // log the error to a file because it's not easy to get info about these Java errors back to Oscript
        try{
            logger.logException(e);
        }
        catch(Exception e2){
            e2.printStackTrace();
        }
    }

}
