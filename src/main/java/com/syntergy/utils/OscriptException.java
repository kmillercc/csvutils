package com.syntergy.utils;


import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.util.CsvContext;

import java.util.Map;

/**
 * Created by Kit on 4/3/17.
 */
public class OscriptException extends Exception {

    boolean fatal;

    public boolean isFatal() {
        return fatal;
    }

    public OscriptException(Map<String, Object> status){

        super((String) status.get("errMsg"));
        Boolean fatal = (Boolean) status.get("fatal");

        this.fatal = fatal != null && fatal;

    }

}
