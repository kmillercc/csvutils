package com.syntergy.utils.ColumnHandlers;

import com.opentext.livelink.oml.OScriptObject;
import com.syntergy.utils.CellProcessors.ParseOScriptDate;
import com.syntergy.utils.CellProcessors.ParseOScriptInt;
import com.syntergy.utils.CellProcessors.ParseOScriptVal;
import com.syntergy.utils.ValidationLogger;
import org.apache.commons.lang3.EnumUtils;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.ift.CellProcessor;

import java.util.EnumMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Kit on 3/18/2015.
 */
public class PoCircColHandler extends ColHandler {

    public enum Columns {
        $CURRENTLOCATION, $BORROWEDDATE, $BORROWEDBY, $BORROWEDBYNAME, $BORROWEDBYID, $RETURNDATE, $OBTAINEDBY, $OBTAINEDBYNAME, $OBTAINEDBYID, $BORROWCOMMENT
    }

    private final EnumMap<Columns, CellProcessor> cellProcessorMap;
    private static final Map<String, Columns> enumMap = EnumUtils.getEnumMap(Columns.class);

    public PoCircColHandler(OScriptObject validateWrapper, String validateFuncName, List<String> dateFormats, ValidationLogger logger){
        cellProcessorMap = new EnumMap<Columns, CellProcessor>(Columns.class);

        cellProcessorMap.put(Columns.$CURRENTLOCATION, new Optional(new ParseOScriptVal(validateWrapper, Columns.$CURRENTLOCATION, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$BORROWEDDATE, new Optional(new ParseOScriptDate(Columns.$BORROWEDDATE, dateFormats, new ParseOScriptVal(validateWrapper, Columns.$BORROWEDDATE, validateFuncName, logger), logger)));
        cellProcessorMap.put(Columns.$BORROWEDBY, new Optional(new ParseOScriptVal(validateWrapper, Columns.$BORROWEDBY, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$BORROWEDBYNAME, new Optional(new ParseOScriptVal(validateWrapper, Columns.$BORROWEDBY, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$BORROWEDBYID, new Optional(new ParseOScriptVal(validateWrapper, Columns.$BORROWEDBYID, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$RETURNDATE, new Optional(new ParseOScriptDate(Columns.$RETURNDATE, dateFormats, new ParseOScriptVal(validateWrapper, Columns.$RETURNDATE, validateFuncName, logger), logger)));
        cellProcessorMap.put(Columns.$OBTAINEDBY, new Optional(new ParseOScriptVal(validateWrapper, Columns.$OBTAINEDBY, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$OBTAINEDBYNAME, new Optional(new ParseOScriptVal(validateWrapper, Columns.$OBTAINEDBYNAME, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$OBTAINEDBYID, new Optional(new ParseOScriptInt(Columns.$OBTAINEDBYID, new ParseOScriptVal(validateWrapper, Columns.$OBTAINEDBYID, validateFuncName, logger), logger)));
        cellProcessorMap.put(Columns.$BORROWCOMMENT, new Optional(new ParseOScriptVal(validateWrapper, Columns.$BORROWCOMMENT, validateFuncName, logger)));

    }

    public static boolean contains(String key){
        return enumMap.containsKey(key);
    }

    public CellProcessor getProcessor(String key){
        return cellProcessorMap.get(Columns.valueOf(key));
    }

    public static Columns[] getColumns(){
        return Columns.values();
    }
}
