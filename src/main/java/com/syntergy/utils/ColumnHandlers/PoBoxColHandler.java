package com.syntergy.utils.ColumnHandlers;

import com.opentext.livelink.oml.OScriptObject;
import com.syntergy.utils.CellProcessors.ParseOScriptVal;
import com.syntergy.utils.ValidationLogger;
import org.apache.commons.lang3.EnumUtils;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.ift.CellProcessor;

import java.util.EnumMap;
import java.util.Map;

/**
 * Created by Kit on 3/18/2015.
 */
public class PoBoxColHandler extends ColHandler {

    public enum Columns {
        $BOXID, $BOXPATH, $BOX
    }

    private final EnumMap<Columns, CellProcessor> cellProcessorMap;
    private static final Map<String, Columns> enumMap = EnumUtils.getEnumMap(Columns.class);

    public PoBoxColHandler(OScriptObject validateWrapper, String validateFuncName, ValidationLogger logger){
        cellProcessorMap = new EnumMap<Columns, CellProcessor>(Columns.class);
        cellProcessorMap.put(Columns.$BOX, new Optional(new ParseOScriptVal(validateWrapper, Columns.$BOX, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$BOXID, new Optional(new ParseOScriptVal(validateWrapper, Columns.$BOXID, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$BOXPATH, new Optional(new ParseOScriptVal(validateWrapper, Columns.$BOXPATH, validateFuncName, logger)));
    }

    public static boolean contains(String key){
        return enumMap.containsKey(key);
    }

    public CellProcessor getProcessor(String key){
        return cellProcessorMap.get(Columns.valueOf(key));
    }

    public static Columns[] getColumns(){
        return Columns.values();
    }
}
