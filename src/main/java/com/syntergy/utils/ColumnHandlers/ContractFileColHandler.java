package com.syntergy.utils.ColumnHandlers;

import com.opentext.livelink.oml.OScriptObject;
import com.syntergy.utils.CellProcessors.ParseOScriptDate;
import com.syntergy.utils.CellProcessors.ParseOScriptVal;
import com.syntergy.utils.ValidationLogger;
import org.apache.commons.lang3.EnumUtils;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.ift.CellProcessor;

import java.util.EnumMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Kit on 3/18/2015.
 */
public class ContractFileColHandler extends ColHandler {

    public enum Columns {
        $CMREFNR, $CMCONTRACTTYPE, $CFMASTERAGREEMENT, $CFEFFECTIVE,$CFEARLYTERMINATION,$CFCURRENTEND,$CFTERMINATED
    }

    private final EnumMap<Columns, CellProcessor> cellProcessorMap;
    private static final Map<String, Columns> enumMap = EnumUtils.getEnumMap(Columns.class);

    public ContractFileColHandler(OScriptObject validateWrapper, String validateFuncName, List<String> dateFormats, ValidationLogger logger){
        cellProcessorMap = new EnumMap<Columns, CellProcessor>(Columns.class);

        cellProcessorMap.put(Columns.$CFEFFECTIVE, new Optional(new ParseOScriptDate(Columns.$CFEFFECTIVE, dateFormats, new ParseOScriptVal(validateWrapper, Columns.$CFEFFECTIVE, validateFuncName, logger), logger)));
        cellProcessorMap.put(Columns.$CFEARLYTERMINATION, new Optional(new ParseOScriptDate(Columns.$CFEARLYTERMINATION, dateFormats, new ParseOScriptVal(validateWrapper, Columns.$CFEARLYTERMINATION, validateFuncName, logger), logger)));
        cellProcessorMap.put(Columns.$CFCURRENTEND, new Optional(new ParseOScriptDate(Columns.$CFCURRENTEND, dateFormats, new ParseOScriptVal(validateWrapper, Columns.$CFCURRENTEND, validateFuncName, logger), logger)));
        cellProcessorMap.put(Columns.$CFTERMINATED, new Optional(new ParseOScriptVal(validateWrapper, Columns.$CFTERMINATED, validateFuncName, logger)));

        cellProcessorMap.put(Columns.$CMREFNR, new Optional(new ParseOScriptVal(validateWrapper, Columns.$CMREFNR, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$CMCONTRACTTYPE, new Optional(new ParseOScriptVal(validateWrapper, Columns.$CMCONTRACTTYPE, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$CFMASTERAGREEMENT, new Optional(new ParseOScriptVal(validateWrapper, Columns.$CFMASTERAGREEMENT, validateFuncName, logger)));



    }

    public static boolean contains(String key){

        return enumMap.containsKey(key);

    }

    public CellProcessor getProcessor(String key){
        return cellProcessorMap.get(Columns.valueOf(key));
    }

    public static Columns[] getColumns(){
        return Columns.values();
    }
}
