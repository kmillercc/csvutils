package com.syntergy.utils.ColumnHandlers;

import com.opentext.livelink.oml.OScriptObject;
import com.syntergy.utils.CellProcessors.ParseOScriptInt;
import com.syntergy.utils.CellProcessors.ParseOScriptVal;
import com.syntergy.utils.ValidationLogger;
import org.apache.commons.lang3.EnumUtils;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.Trim;
import org.supercsv.cellprocessor.ift.CellProcessor;

import java.util.EnumMap;
import java.util.Map;

/**
 * Created by Kit on 3/18/2015.
 */
public class UgColHandler extends ColHandler {

    public enum Columns {
        $ID, $OWNERID, $TYPE, $SPACEID, $NAME, $USERDATA, $LEADERID, $LEADER, $GROUPID, $USERPRIVILEGES, $LASTNAME, $MIDDLENAME, $FIRSTNAME, $MAILADDRESS, $CONTACT, $TITLE, $MEMBERS, $USERPWD, $PASSWORD
    }

    private final EnumMap<Columns, CellProcessor> cellProcessorMap;
    private static final Map<String, Columns> enumMap = EnumUtils.getEnumMap(Columns.class);

    public UgColHandler(OScriptObject validateWrapper, String validateFuncName, ValidationLogger logger){
        cellProcessorMap = new EnumMap<Columns, CellProcessor>(Columns.class);
        cellProcessorMap.put(Columns.$ID, new Optional(new ParseOScriptInt(Columns.$ID, new ParseOScriptVal(validateWrapper, Columns.$ID, validateFuncName, logger), logger)));
        cellProcessorMap.put(Columns.$OWNERID, new Optional(new ParseOScriptInt(Columns.$OWNERID, new ParseOScriptVal(validateWrapper, Columns.$OWNERID, validateFuncName, logger), logger)));
        cellProcessorMap.put(Columns.$TYPE, new Optional(new ParseOScriptInt(Columns.$TYPE, new ParseOScriptVal(validateWrapper, Columns.$TYPE, validateFuncName, logger), logger)));
        cellProcessorMap.put(Columns.$SPACEID, new Optional(new ParseOScriptInt(Columns.$SPACEID, new ParseOScriptVal(validateWrapper, Columns.$SPACEID, validateFuncName, logger), logger)));
        cellProcessorMap.put(Columns.$NAME, new Optional(new Trim(new ParseOScriptVal(validateWrapper, Columns.$NAME, validateFuncName, logger))));
        cellProcessorMap.put(Columns.$USERDATA, new Optional(new ParseOScriptVal(validateWrapper, Columns.$USERDATA, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$LEADERID, new Optional(new ParseOScriptVal(validateWrapper, Columns.$LEADERID, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$LEADER, new Optional(new ParseOScriptVal(validateWrapper, Columns.$LEADER, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$GROUPID, new Optional(new ParseOScriptInt(Columns.$GROUPID, new ParseOScriptVal(validateWrapper, Columns.$GROUPID, validateFuncName, logger), logger)));
        cellProcessorMap.put(Columns.$USERPRIVILEGES, new Optional(new ParseOScriptVal(validateWrapper, Columns.$USERPRIVILEGES, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$LASTNAME, new Optional(new Trim(new ParseOScriptVal(validateWrapper, Columns.$LASTNAME, validateFuncName, logger))));
        cellProcessorMap.put(Columns.$MIDDLENAME, new Optional(new Trim(new ParseOScriptVal(validateWrapper, Columns.$MIDDLENAME, validateFuncName, logger))));
        cellProcessorMap.put(Columns.$FIRSTNAME, new Optional(new Trim(new ParseOScriptVal(validateWrapper, Columns.$FIRSTNAME, validateFuncName, logger))));
        cellProcessorMap.put(Columns.$MAILADDRESS, new Optional(new Trim(new ParseOScriptVal(validateWrapper, Columns.$MAILADDRESS, validateFuncName, logger))));
        cellProcessorMap.put(Columns.$CONTACT, new Optional(new Trim(new ParseOScriptVal(validateWrapper, Columns.$CONTACT, validateFuncName, logger))));
        cellProcessorMap.put(Columns.$TITLE, new Optional(new Trim(new ParseOScriptVal(validateWrapper, Columns.$TITLE, validateFuncName, logger))));
        cellProcessorMap.put(Columns.$USERPWD, new Optional(new ParseOScriptVal(validateWrapper, Columns.$USERPWD, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$PASSWORD, new Optional(new ParseOScriptVal(validateWrapper, Columns.$PASSWORD, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$MEMBERS, new Optional(new Trim(new ParseOScriptVal(validateWrapper, Columns.$MEMBERS, validateFuncName, logger))));
    }

    public static boolean contains(String key){
        return enumMap.containsKey(key);
    }

    public CellProcessor getProcessor(String key){
        return cellProcessorMap.get(Columns.valueOf(key));
    }

    public static Columns[] getColumns(){
        return Columns.values();
    }
}
