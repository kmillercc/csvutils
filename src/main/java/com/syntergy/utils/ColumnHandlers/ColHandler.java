package com.syntergy.utils.ColumnHandlers;

import org.supercsv.cellprocessor.ift.CellProcessor;

/**
 * Created by Kit on 3/19/2015.
 */
public abstract class ColHandler{

    public abstract CellProcessor getProcessor(String key);

}
