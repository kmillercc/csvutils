package com.syntergy.utils.ColumnHandlers;

import com.opentext.livelink.oml.OScriptObject;
import com.syntergy.utils.CellProcessors.ParseOScriptVal;
import com.syntergy.utils.ValidationLogger;
import org.apache.commons.lang3.EnumUtils;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.ift.CellProcessor;

import java.util.EnumMap;
import java.util.Map;

/**
 * Created by Kit on 3/18/2015.
 */
public class PoLocatorColHandler extends ColHandler {

    public enum Columns {
        $AREA, $LOCATOR, $BOXLOCATOR, $FACILITY
    }

    private final EnumMap<Columns, CellProcessor> cellProcessorMap;
    private static final Map<String, Columns> enumMap = EnumUtils.getEnumMap(Columns.class);

    public PoLocatorColHandler(OScriptObject validateWrapper, String validateFuncName, ValidationLogger logger){
        cellProcessorMap = new EnumMap<Columns, CellProcessor>(Columns.class);
        cellProcessorMap.put(Columns.$AREA, new Optional(new ParseOScriptVal(validateWrapper, Columns.$AREA, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$FACILITY, new Optional(new ParseOScriptVal(validateWrapper, Columns.$FACILITY, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$BOXLOCATOR, new Optional(new ParseOScriptVal(validateWrapper, Columns.$BOXLOCATOR, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$LOCATOR, new Optional(new ParseOScriptVal(validateWrapper, Columns.$LOCATOR, validateFuncName, logger)));
    }

    public static boolean contains(String key){
        return enumMap.containsKey(key);
    }

    public CellProcessor getProcessor(String key){
        return cellProcessorMap.get(Columns.valueOf(key));
    }

    public static Columns[] getColumns(){
        return Columns.values();
    }
}
