package com.syntergy.utils.ColumnHandlers;

import com.opentext.livelink.oml.OScriptObject;
import com.syntergy.utils.CellProcessors.ParseOScriptInt;
import com.syntergy.utils.CellProcessors.ParseOScriptVal;
import com.syntergy.utils.ValidationLogger;
import org.apache.commons.lang3.EnumUtils;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.ift.CellProcessor;

import java.util.EnumMap;
import java.util.Map;

/**
 * Created by Kit on 3/18/2015.
 */
public class PoCreateColHandler extends ColHandler {

    public enum Columns {
        $MEDIATYPEFIELDS, $UNIQUEID, $SELECTEDMEDIA, $BOXCLIENT, $GENERATELABEL, $NUMLABELS, $PHYSITEMTYPE, $NUMCOPIES, $COPYNO
    }

    private final EnumMap<Columns, CellProcessor> cellProcessorMap;
    private static final Map<String, Columns> enumMap = EnumUtils.getEnumMap(Columns.class);

    public PoCreateColHandler(OScriptObject validateWrapper, String validateFuncName, ValidationLogger logger){
        cellProcessorMap = new EnumMap<Columns, CellProcessor>(Columns.class);
        cellProcessorMap.put(Columns.$NUMCOPIES, new Optional(new ParseOScriptInt(Columns.$NUMCOPIES, new ParseOScriptVal(validateWrapper, Columns.$NUMCOPIES, validateFuncName, logger), logger)));
        cellProcessorMap.put(Columns.$COPYNO, new Optional(new ParseOScriptInt(Columns.$COPYNO, new ParseOScriptVal(validateWrapper, Columns.$COPYNO, validateFuncName, logger), logger)));

        cellProcessorMap.put(Columns.$UNIQUEID, new Optional(new ParseOScriptVal(validateWrapper, Columns.$UNIQUEID, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$SELECTEDMEDIA, new Optional(new ParseOScriptVal(validateWrapper, Columns.$SELECTEDMEDIA, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$PHYSITEMTYPE, new Optional(new ParseOScriptVal(validateWrapper, Columns.$PHYSITEMTYPE, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$BOXCLIENT, new Optional(new ParseOScriptVal(validateWrapper, Columns.$BOXCLIENT, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$GENERATELABEL, new Optional(new ParseOScriptVal(validateWrapper, Columns.$GENERATELABEL, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$NUMLABELS, new Optional(new ParseOScriptInt(Columns.$NUMLABELS, new ParseOScriptVal(validateWrapper, Columns.$NUMLABELS, validateFuncName, logger), logger)));

    }

    public static boolean contains(String key){
        return enumMap.containsKey(key);
    }

    public CellProcessor getProcessor(String key){
        return cellProcessorMap.get(Columns.valueOf(key));
    }

    public static Columns[] getColumns(){
        return Columns.values();
    }
}
