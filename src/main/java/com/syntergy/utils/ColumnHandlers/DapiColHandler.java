package com.syntergy.utils.ColumnHandlers;

import com.opentext.livelink.oml.OScriptObject;
import com.syntergy.utils.CellProcessors.ParseOScriptDate;
import com.syntergy.utils.CellProcessors.ParseOScriptInt;
import com.syntergy.utils.CellProcessors.ParseOScriptVal;
import com.syntergy.utils.ValidationLogger;
import org.apache.commons.lang3.EnumUtils;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.Trim;
import org.supercsv.cellprocessor.ift.CellProcessor;

import java.util.EnumMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Kit on 3/18/2015.
 */
public class DapiColHandler extends ColHandler {

    public enum Columns {
        $CREATEDATE, $MODIFYDATE, $CREATOR, $DESCRIPTION, $CATALOG
    }

    private final EnumMap<Columns, CellProcessor> cellProcessorMap;
    private static final Map<String, Columns> enumMap = EnumUtils.getEnumMap(Columns.class);

    public DapiColHandler(OScriptObject validateWrapper, String validateFuncName, List<String> dateFormats, ValidationLogger logger){
        cellProcessorMap = new EnumMap<Columns, CellProcessor>(Columns.class);

        cellProcessorMap.put(Columns.$CREATEDATE, new Optional(new ParseOScriptDate(Columns.$CREATEDATE, dateFormats, new ParseOScriptVal(validateWrapper, Columns.$CREATEDATE, validateFuncName, logger), logger)));
        cellProcessorMap.put(Columns.$MODIFYDATE, new Optional(new ParseOScriptDate(Columns.$MODIFYDATE, dateFormats, new ParseOScriptVal(validateWrapper, Columns.$MODIFYDATE, validateFuncName, logger), logger)));
        cellProcessorMap.put(Columns.$CREATOR, new Optional(new ParseOScriptVal(validateWrapper, Columns.$CREATOR, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$DESCRIPTION, new Optional(new Trim(new ParseOScriptVal(validateWrapper, Columns.$DESCRIPTION, validateFuncName, logger))));
        cellProcessorMap.put(Columns.$CATALOG, new Optional(new ParseOScriptInt(Columns.$CATALOG,new ParseOScriptVal(validateWrapper, Columns.$CATALOG, validateFuncName, logger), logger)));
    }

    public static boolean contains(String key){

        return enumMap.containsKey(key);

    }

    public CellProcessor getProcessor(String key){
        return cellProcessorMap.get(Columns.valueOf(key));
    }

    public static Columns[] getColumns(){
        return Columns.values();
    }
}
