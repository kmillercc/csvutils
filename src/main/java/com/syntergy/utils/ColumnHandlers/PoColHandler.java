package com.syntergy.utils.ColumnHandlers;

import com.opentext.livelink.oml.OScriptObject;
import com.syntergy.utils.CellProcessors.ParseOScriptDate;
import com.syntergy.utils.CellProcessors.ParseOScriptVal;
import com.syntergy.utils.ValidationLogger;
import org.apache.commons.lang3.EnumUtils;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.ift.CellProcessor;

import java.util.EnumMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Kit on 3/18/2015.
 */
public class PoColHandler extends ColHandler {

    public enum Columns {
        $CLIENT, $CLIENTID, $CLIENTNAME, $FROMDATE, $KEYWORDS, $LOCATION, $HOMELOCATION,
        $BOXLOCATOR, $LOCATORTYPE, $OFFSITESTORID, $REFERENCERATE, $TODATE, $TEMPORARYID,
        $MEDIATYPEFIELDS
    }

    private final EnumMap<Columns, CellProcessor> cellProcessorMap;
    private static final Map<String, Columns> enumMap = EnumUtils.getEnumMap(Columns.class);

    public PoColHandler(OScriptObject validateWrapper, String validateFuncName, List<String> dateFormats, ValidationLogger logger){
        cellProcessorMap = new EnumMap<Columns, CellProcessor>(Columns.class);
        cellProcessorMap.put(Columns.$CLIENT, new Optional(new ParseOScriptVal(validateWrapper, Columns.$CLIENT, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$CLIENTID, new Optional(new ParseOScriptVal(validateWrapper, Columns.$CLIENTID, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$CLIENTNAME, new Optional(new ParseOScriptVal(validateWrapper, Columns.$CLIENTNAME, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$FROMDATE, new Optional(new ParseOScriptDate(Columns.$FROMDATE, dateFormats, new ParseOScriptVal(validateWrapper, Columns.$FROMDATE, validateFuncName, logger), logger)));
        cellProcessorMap.put(Columns.$KEYWORDS, new Optional(new ParseOScriptVal(validateWrapper, Columns.$KEYWORDS, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$LOCATION, new Optional(new ParseOScriptVal(validateWrapper, Columns.$LOCATION, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$HOMELOCATION, new Optional(new ParseOScriptVal(validateWrapper, Columns.$HOMELOCATION, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$LOCATORTYPE, new Optional(new ParseOScriptVal(validateWrapper, Columns.$LOCATORTYPE, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$OFFSITESTORID, new Optional(new ParseOScriptVal(validateWrapper, Columns.$OFFSITESTORID, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$REFERENCERATE, new Optional(new ParseOScriptVal(validateWrapper, Columns.$REFERENCERATE, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$TODATE, new Optional(new ParseOScriptDate(Columns.$TODATE, dateFormats, new ParseOScriptVal(validateWrapper, Columns.$TODATE, validateFuncName, logger), logger)));
        cellProcessorMap.put(Columns.$TEMPORARYID, new Optional(new ParseOScriptVal(validateWrapper, Columns.$TEMPORARYID, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$MEDIATYPEFIELDS, new Optional(new ParseOScriptVal(validateWrapper, Columns.$MEDIATYPEFIELDS, validateFuncName, logger)));
    }

    public static boolean contains(String key){
        return enumMap.containsKey(key);
    }

    public CellProcessor getProcessor(String key){
        return cellProcessorMap.get(Columns.valueOf(key));
    }

    public static Columns[] getColumns(){
        return Columns.values();
    }
}
