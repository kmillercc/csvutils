package com.syntergy.utils.ColumnHandlers;

import com.opentext.livelink.oml.OScriptObject;
import com.syntergy.utils.CellProcessors.ParseOScriptDate;
import com.syntergy.utils.CellProcessors.ParseOScriptInt;
import com.syntergy.utils.CellProcessors.ParseOScriptVal;
import com.syntergy.utils.ValidationLogger;
import org.apache.commons.lang3.EnumUtils;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.ift.CellProcessor;

import java.util.EnumMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Kit on 3/18/2015.
 */
public class EmailColHandler extends ColHandler {

    public enum Columns {
        $EMAILSUBJECT, $EMAILTO, $EMAILFROM, $EMAILONBEHALFOF, $EMAILCC, $EMAILBCC, $EMAILRECEIVEDDATE, $EMAILSENTDATE, $EMAILHASATTACHMENTS, $EMAILBODYFORMAT, $EMAILVERSION, $EMAILPARTICIPANTS
    }

    private final EnumMap<Columns, CellProcessor> cellProcessorMap;
    private static final Map<String, Columns> enumMap = EnumUtils.getEnumMap(Columns.class);

    public EmailColHandler(OScriptObject validateWrapper, String validateFuncName, List<String> dateFormats, ValidationLogger logger){
        cellProcessorMap = new EnumMap<Columns, CellProcessor>(Columns.class);

        cellProcessorMap.put(Columns.$EMAILSUBJECT, new Optional(new ParseOScriptVal(validateWrapper, Columns.$EMAILSUBJECT, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$EMAILTO, new Optional(new ParseOScriptVal(validateWrapper, Columns.$EMAILTO, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$EMAILFROM, new Optional(new ParseOScriptVal(validateWrapper, Columns.$EMAILFROM, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$EMAILONBEHALFOF, new Optional(new ParseOScriptVal(validateWrapper, Columns.$EMAILONBEHALFOF, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$EMAILCC, new Optional(new ParseOScriptVal(validateWrapper, Columns.$EMAILCC, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$EMAILBCC, new Optional(new ParseOScriptVal(validateWrapper, Columns.$EMAILBCC, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$EMAILPARTICIPANTS, new Optional(new ParseOScriptVal(validateWrapper, Columns.$EMAILPARTICIPANTS, validateFuncName, logger)));

        // date columns
        cellProcessorMap.put(Columns.$EMAILRECEIVEDDATE, new Optional(new ParseOScriptDate(Columns.$EMAILRECEIVEDDATE, dateFormats, new ParseOScriptVal(validateWrapper, Columns.$EMAILRECEIVEDDATE, validateFuncName, logger), logger)));
        cellProcessorMap.put(Columns.$EMAILSENTDATE, new Optional(new ParseOScriptDate(Columns.$EMAILSENTDATE, dateFormats, new ParseOScriptVal(validateWrapper, Columns.$EMAILSENTDATE, validateFuncName, logger), logger)));

        // integer columns
        cellProcessorMap.put(Columns.$EMAILHASATTACHMENTS, new Optional(new ParseOScriptInt(Columns.$EMAILHASATTACHMENTS,new ParseOScriptVal(validateWrapper, Columns.$EMAILHASATTACHMENTS, validateFuncName, logger), logger)));
        cellProcessorMap.put(Columns.$EMAILBODYFORMAT, new Optional(new ParseOScriptInt(Columns.$EMAILBODYFORMAT,new ParseOScriptVal(validateWrapper, Columns.$EMAILBODYFORMAT, validateFuncName, logger), logger)));
        cellProcessorMap.put(Columns.$EMAILVERSION, new Optional(new ParseOScriptInt(Columns.$EMAILVERSION,new ParseOScriptVal(validateWrapper, Columns.$EMAILVERSION, validateFuncName, logger), logger)));

    }

    public static boolean contains(String key){
        return enumMap.containsKey(key);
    }

    public CellProcessor getProcessor(String key){
        return cellProcessorMap.get(Columns.valueOf(key));
    }

    public static Columns[] getColumns(){
        return Columns.values();
    }
}
