package com.syntergy.utils.ColumnHandlers;

import com.opentext.livelink.oml.OScriptObject;
import com.syntergy.utils.CellProcessors.ParseOScriptDate;
import com.syntergy.utils.CellProcessors.ParseOScriptInt;
import com.syntergy.utils.CellProcessors.ParseOScriptVal;
import com.syntergy.utils.ValidationLogger;
import org.apache.commons.lang3.EnumUtils;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.ift.CellProcessor;

import java.util.EnumMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Kit on 3/18/2015.
 */
public class VersionColHandler extends ColHandler {

    public enum Columns {
        $VERSIONNUM,$VERDESCRIPTION,$VERMAJOR,$VERMINOR,$VERFILENAME,$VERFILESIZE,$VERCREATEDATE,$VERMODIFIEDDATE,$VERMIMETYPE,$VERCREATOR,$VERSION, $VERSIONDESCRIPTION, $MAJOR, $MINOR, $VERSIONFILENAME, $VERSIONFILESIZE, $VERSIONCREATEDATE, $VERSIONMODIFIEDDATE, $VERSIONMIMETYPE, $VERSIONCREATOR
    }

    private final EnumMap<Columns, CellProcessor> cellProcessorMap;
    private static final Map<String, Columns> enumMap = EnumUtils.getEnumMap(Columns.class);

    public VersionColHandler(OScriptObject validateWrapper, String validateFuncName, List<String> dateFormats, ValidationLogger logger){
        cellProcessorMap = new EnumMap<Columns, CellProcessor>(Columns.class);

        cellProcessorMap.put(Columns.$VERSIONNUM, new Optional(new ParseOScriptInt(Columns.$VERSIONNUM, new ParseOScriptVal(validateWrapper, Columns.$VERSIONNUM, validateFuncName, logger), logger)));
        cellProcessorMap.put(Columns.$VERSION, new Optional(new ParseOScriptInt(Columns.$VERSION, new ParseOScriptVal(validateWrapper, Columns.$VERSION, validateFuncName, logger), logger)));

        cellProcessorMap.put(Columns.$VERDESCRIPTION, new Optional(new ParseOScriptVal(validateWrapper, Columns.$VERDESCRIPTION, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$VERSIONDESCRIPTION, new Optional(new ParseOScriptVal(validateWrapper, Columns.$VERSIONDESCRIPTION, validateFuncName, logger)));

        cellProcessorMap.put(Columns.$VERMAJOR, new Optional(new ParseOScriptVal(validateWrapper, Columns.$VERMAJOR, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$MAJOR, new Optional(new ParseOScriptVal(validateWrapper, Columns.$MAJOR, validateFuncName, logger)));

        cellProcessorMap.put(Columns.$VERMINOR, new Optional(new ParseOScriptVal(validateWrapper, Columns.$VERMINOR, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$MINOR, new Optional(new ParseOScriptVal(validateWrapper, Columns.$MINOR, validateFuncName, logger)));

        cellProcessorMap.put(Columns.$VERFILENAME, new Optional(new ParseOScriptVal(validateWrapper, Columns.$VERFILENAME, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$VERSIONFILENAME, new Optional(new ParseOScriptVal(validateWrapper, Columns.$VERSIONFILENAME, validateFuncName, logger)));

        cellProcessorMap.put(Columns.$VERFILESIZE, new Optional(new ParseOScriptInt(Columns.$VERFILESIZE, new ParseOScriptVal(validateWrapper, Columns.$VERFILESIZE, validateFuncName, logger), logger)));
        cellProcessorMap.put(Columns.$VERSIONFILESIZE, new Optional(new ParseOScriptInt(Columns.$VERSIONFILESIZE, new ParseOScriptVal(validateWrapper, Columns.$VERSIONFILESIZE, validateFuncName, logger), logger)));

        cellProcessorMap.put(Columns.$VERCREATEDATE, new Optional(new ParseOScriptDate(Columns.$VERCREATEDATE, dateFormats, new ParseOScriptVal(validateWrapper, Columns.$VERCREATEDATE, validateFuncName, logger), logger)));
        cellProcessorMap.put(Columns.$VERSIONCREATEDATE, new Optional(new ParseOScriptDate(Columns.$VERSIONCREATEDATE, dateFormats, new ParseOScriptVal(validateWrapper, Columns.$VERSIONCREATEDATE, validateFuncName, logger), logger)));

        cellProcessorMap.put(Columns.$VERMODIFIEDDATE, new Optional(new ParseOScriptDate(Columns.$VERMODIFIEDDATE, dateFormats, new ParseOScriptVal(validateWrapper, Columns.$VERMODIFIEDDATE, validateFuncName, logger), logger)));
        cellProcessorMap.put(Columns.$VERSIONMODIFIEDDATE, new Optional(new ParseOScriptDate(Columns.$VERSIONMODIFIEDDATE, dateFormats, new ParseOScriptVal(validateWrapper, Columns.$VERSIONMODIFIEDDATE, validateFuncName, logger), logger)));

        cellProcessorMap.put(Columns.$VERMIMETYPE, new Optional(new ParseOScriptVal(validateWrapper, Columns.$VERMIMETYPE, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$VERSIONMIMETYPE, new Optional(new ParseOScriptVal(validateWrapper, Columns.$VERSIONMIMETYPE, validateFuncName, logger)));

        cellProcessorMap.put(Columns.$VERCREATOR, new Optional(new ParseOScriptVal(validateWrapper, Columns.$VERCREATOR, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$VERSIONCREATOR, new Optional(new ParseOScriptVal(validateWrapper, Columns.$VERSIONCREATOR, validateFuncName, logger)));
    }

    public static boolean contains(String key){
        return enumMap.containsKey(key);
    }

    public CellProcessor getProcessor(String key){
        return cellProcessorMap.get(Columns.valueOf(key));
    }

    public static Columns[] getColumns(){
        return Columns.values();
    }
}
