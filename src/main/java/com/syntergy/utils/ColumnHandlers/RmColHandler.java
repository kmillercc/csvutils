package com.syntergy.utils.ColumnHandlers;

import com.opentext.livelink.oml.OScriptObject;
import com.syntergy.utils.CellProcessors.ParseOScriptDate;
import com.syntergy.utils.CellProcessors.ParseOScriptVal;
import com.syntergy.utils.ValidationLogger;
import org.apache.commons.lang3.EnumUtils;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.ift.CellProcessor;

import java.util.EnumMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Kit on 3/18/2015.
 */
public class RmColHandler extends ColHandler {

    public enum Columns {
        $RDCLASSIFICATION, $RDSUBJECT, $RDOFFICIAL, $RDRECORDDATE, $RDSTATUS, $RDSTATUSDATE, $RDESSENTIAL, $RDRECEIVEDDATE, $RDSTORAGEMEDIUM, $RDACCESSION, $RDAUTHOR,
        $RDORIGINATOR, $RDADDRESSEE, $RDOTHERADDRESSEE, $RDORGANIZATION, $RDORIGINATINGORGANIZATION, $RDUPDATECYCLEPERIOD, $RDNEXTREVIEWDATE, $RDLASTREVIEWDATE, $RDRSI, $FILENUMBER
    }

    private final EnumMap<Columns, CellProcessor> cellProcessorMap;
    private static final Map<String, Columns> enumMap = EnumUtils.getEnumMap(Columns.class);

    public RmColHandler(OScriptObject validateWrapper, String validateFuncName, List<String> dateFormats, ValidationLogger logger){
        cellProcessorMap = new EnumMap<Columns, CellProcessor>(Columns.class);
        cellProcessorMap.put(Columns.$FILENUMBER, new Optional(new ParseOScriptVal(validateWrapper, Columns.$FILENUMBER, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$RDCLASSIFICATION, new Optional(new ParseOScriptVal(validateWrapper, Columns.$RDCLASSIFICATION, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$RDSUBJECT, new Optional(new ParseOScriptVal(validateWrapper, Columns.$RDSUBJECT, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$RDOFFICIAL, new Optional(new ParseOScriptVal(validateWrapper, Columns.$RDOFFICIAL, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$RDRECORDDATE, new Optional(new ParseOScriptDate(Columns.$RDRECORDDATE, dateFormats, new ParseOScriptVal(validateWrapper, Columns.$RDRECORDDATE, validateFuncName, logger), logger)));
        cellProcessorMap.put(Columns.$RDSTATUS, new Optional(new ParseOScriptVal(validateWrapper, Columns.$RDSTATUS, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$RDSTATUSDATE, new Optional(new ParseOScriptDate(Columns.$RDSTATUSDATE, dateFormats, new ParseOScriptVal(validateWrapper, Columns.$RDSTATUSDATE, validateFuncName, logger), logger)));
        cellProcessorMap.put(Columns.$RDESSENTIAL, new Optional(new ParseOScriptVal(validateWrapper, Columns.$RDESSENTIAL, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$RDRECEIVEDDATE, new Optional(new ParseOScriptDate(Columns.$RDRECEIVEDDATE, dateFormats, new ParseOScriptVal(validateWrapper, Columns.$RDRECEIVEDDATE, validateFuncName, logger), logger)));
        cellProcessorMap.put(Columns.$RDSTORAGEMEDIUM, new Optional(new ParseOScriptVal(validateWrapper, Columns.$RDSTORAGEMEDIUM, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$RDACCESSION, new Optional(new ParseOScriptVal(validateWrapper, Columns.$RDACCESSION, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$RDAUTHOR, new Optional(new ParseOScriptVal(validateWrapper, Columns.$RDAUTHOR, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$RDORIGINATOR, new Optional(new ParseOScriptVal(validateWrapper, Columns.$RDORIGINATOR, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$RDADDRESSEE, new Optional(new ParseOScriptVal(validateWrapper, Columns.$RDADDRESSEE, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$RDOTHERADDRESSEE, new Optional(new ParseOScriptVal(validateWrapper, Columns.$RDOTHERADDRESSEE, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$RDORGANIZATION, new Optional(new ParseOScriptVal(validateWrapper, Columns.$RDORGANIZATION, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$RDORIGINATINGORGANIZATION, new Optional(new ParseOScriptVal(validateWrapper, Columns.$RDORIGINATINGORGANIZATION, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$RDUPDATECYCLEPERIOD, new Optional(new ParseOScriptVal(validateWrapper, Columns.$RDUPDATECYCLEPERIOD, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$RDNEXTREVIEWDATE, new Optional(new ParseOScriptDate(Columns.$RDNEXTREVIEWDATE, dateFormats, new ParseOScriptVal(validateWrapper, Columns.$RDNEXTREVIEWDATE, validateFuncName, logger), logger)));
        cellProcessorMap.put(Columns.$RDLASTREVIEWDATE, new Optional(new ParseOScriptDate(Columns.$RDLASTREVIEWDATE, dateFormats, new ParseOScriptVal(validateWrapper, Columns.$RDLASTREVIEWDATE, validateFuncName, logger), logger)));
        cellProcessorMap.put(Columns.$RDRSI, new Optional(new ParseOScriptVal(validateWrapper, Columns.$RDRSI, validateFuncName, logger)));
    }

    public static boolean contains(String key){
        return enumMap.containsKey(key);
    }

    public CellProcessor getProcessor(String key){
        return cellProcessorMap.get(Columns.valueOf(key));
    }

    public static Columns[] getColumns(){
        return Columns.values();
    }
}
