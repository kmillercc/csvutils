package com.syntergy.utils.ColumnHandlers;

import com.opentext.livelink.oml.OScriptObject;
import com.syntergy.utils.CellProcessors.ParseOScriptInt;
import com.syntergy.utils.CellProcessors.ParseOScriptVal;
import com.syntergy.utils.ValidationLogger;
import org.apache.commons.lang3.EnumUtils;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.Trim;
import org.supercsv.cellprocessor.ift.CellProcessor;

import java.util.EnumMap;
import java.util.Map;

/**
 * Created by Kit on 3/18/2015.
 */
public class CommonColHandler extends ColHandler {

    public enum Columns {
        $ACTION, $OBJECTNAME, $TARGETPATH, $TOPLEVELPATH, $OBJECTID, $OBJECTTYPE, $SOURCEPATH, $FILENAME, $NEWNAME, $VERSIONSTOKEEP, $NEWPARENTID, $PARENTID, $VOLUMEID, $NICKNAME, $ORDER, $TEMPLATE, $ALLOWBREAKBEFORE, $ADVVERSIONING, $SHORTCUTTARGET, $TARGETURL
    }

    private final EnumMap<Columns, CellProcessor> cellProcessorMap;
    private static final Map<String, Columns> enumMap = EnumUtils.getEnumMap(Columns.class);

    public CommonColHandler(OScriptObject validateWrapper, String validateFuncName, ValidationLogger logger) {
        cellProcessorMap = new EnumMap<Columns, CellProcessor>(Columns.class);

        // all of these get handled later in the OScript code
        cellProcessorMap.put(Columns.$ACTION, new Optional(new Trim(new ParseOScriptVal(validateWrapper, Columns.$ACTION, validateFuncName, logger))));
        cellProcessorMap.put(Columns.$OBJECTNAME, new Optional(new Trim(new ParseOScriptVal(validateWrapper, Columns.$OBJECTNAME, validateFuncName, logger))));
        cellProcessorMap.put(Columns.$TARGETPATH, new Optional(new Trim(new ParseOScriptVal(validateWrapper, Columns.$TARGETPATH, validateFuncName, logger))));
        cellProcessorMap.put(Columns.$TOPLEVELPATH, new Optional(new Trim(new ParseOScriptVal(validateWrapper, Columns.$TOPLEVELPATH, validateFuncName, logger))));
        cellProcessorMap.put(Columns.$OBJECTID, new Optional(new ParseOScriptInt(Columns.$OBJECTID, new ParseOScriptVal(validateWrapper, Columns.$OBJECTID, validateFuncName, logger), logger)));
        cellProcessorMap.put(Columns.$PARENTID, new Optional(new ParseOScriptInt(Columns.$PARENTID, new ParseOScriptVal(validateWrapper, Columns.$PARENTID, validateFuncName, logger), logger)));
        cellProcessorMap.put(Columns.$NEWPARENTID, new Optional(new ParseOScriptInt(Columns.$NEWPARENTID, new ParseOScriptVal(validateWrapper, Columns.$NEWPARENTID, validateFuncName, logger), logger)));
        cellProcessorMap.put(Columns.$VOLUMEID, new Optional(new ParseOScriptInt(Columns.$VOLUMEID, new ParseOScriptVal(validateWrapper, Columns.$VOLUMEID, validateFuncName, logger), logger)));
        cellProcessorMap.put(Columns.$OBJECTTYPE, new Optional(new ParseOScriptInt(Columns.$OBJECTTYPE, new ParseOScriptVal(validateWrapper, Columns.$OBJECTTYPE, validateFuncName, logger), logger)));
        cellProcessorMap.put(Columns.$SOURCEPATH, new Optional(new Trim(new ParseOScriptVal(validateWrapper, Columns.$SOURCEPATH, validateFuncName, logger))));
        cellProcessorMap.put(Columns.$FILENAME, new Optional(new Trim(new ParseOScriptVal(validateWrapper, Columns.$FILENAME, validateFuncName, logger))));
        cellProcessorMap.put(Columns.$NEWNAME, new Optional(new Trim(new ParseOScriptVal(validateWrapper, Columns.$NEWNAME, validateFuncName, logger))));
        cellProcessorMap.put(Columns.$VERSIONSTOKEEP, new Optional(new ParseOScriptInt(Columns.$VERSIONSTOKEEP, new ParseOScriptVal(validateWrapper, Columns.$VERSIONSTOKEEP, validateFuncName, logger), logger)));
        cellProcessorMap.put(Columns.$NICKNAME, new Optional(new Trim(new ParseOScriptVal(validateWrapper, Columns.$NICKNAME, validateFuncName, logger))));
        cellProcessorMap.put(Columns.$ORDER, new Optional(new ParseOScriptInt(Columns.$ORDER, new ParseOScriptVal(validateWrapper, Columns.$ORDER, validateFuncName, logger), logger)));
        cellProcessorMap.put(Columns.$TEMPLATE, new Optional(new ParseOScriptVal(validateWrapper, Columns.$TEMPLATE, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$ALLOWBREAKBEFORE, new Optional(new ParseOScriptVal(validateWrapper, Columns.$ALLOWBREAKBEFORE, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$ADVVERSIONING, new Optional(new ParseOScriptVal(validateWrapper, Columns.$ADVVERSIONING, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$SHORTCUTTARGET, new Optional(new ParseOScriptVal(validateWrapper, Columns.$SHORTCUTTARGET, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$TARGETURL, new Optional(new Trim(new ParseOScriptVal(validateWrapper, Columns.$TARGETURL, validateFuncName, logger))));

    }

    public static boolean contains(String key) {
        return enumMap.containsKey(key);
    }

    public CellProcessor getProcessor(String key) {
        return cellProcessorMap.get(Columns.valueOf(key));
    }

    public static Columns[] getColumns() {
        return Columns.values();
    }

}
