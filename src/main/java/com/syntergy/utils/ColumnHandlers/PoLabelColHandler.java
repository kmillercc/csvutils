package com.syntergy.utils.ColumnHandlers;

import com.opentext.livelink.oml.OScriptObject;
import com.syntergy.utils.CellProcessors.ParseOScriptInt;
import com.syntergy.utils.CellProcessors.ParseOScriptVal;
import com.syntergy.utils.ValidationLogger;
import org.apache.commons.lang3.EnumUtils;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.ift.CellProcessor;

import java.util.EnumMap;
import java.util.Map;

/**
 * Created by Kit on 3/18/2015.
 */
public class PoLabelColHandler extends ColHandler {

    public enum Columns {
        $LABELTYPE, $LABELCOPIES, $NUMCOPIES
    }

    private final EnumMap<Columns, CellProcessor> cellProcessorMap;
    private static final Map<String, Columns> enumMap = EnumUtils.getEnumMap(Columns.class);

    public PoLabelColHandler(OScriptObject validateWrapper, String validateFuncName, ValidationLogger logger){
        cellProcessorMap = new EnumMap<Columns, CellProcessor>(Columns.class);
        cellProcessorMap.put(Columns.$NUMCOPIES, new Optional(new ParseOScriptInt(Columns.$NUMCOPIES, new ParseOScriptVal(validateWrapper, Columns.$NUMCOPIES, validateFuncName, logger), logger)));
        cellProcessorMap.put(Columns.$LABELTYPE, new Optional(new ParseOScriptVal(validateWrapper, Columns.$LABELTYPE, validateFuncName, logger)));
        cellProcessorMap.put(Columns.$LABELCOPIES, new Optional(new ParseOScriptInt(Columns.$LABELCOPIES, new ParseOScriptVal(validateWrapper, Columns.$LABELCOPIES, validateFuncName, logger), logger)));
    }

    public static boolean contains(String key){
        return enumMap.containsKey(key);
    }

    public CellProcessor getProcessor(String key){
        return cellProcessorMap.get(Columns.valueOf(key));
    }

    public static Columns[] getColumns(){
        return Columns.values();
    }
}
