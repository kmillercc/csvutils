package com.syntergy.utils;

import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvMapWriter;
import org.supercsv.io.ICsvListWriter;
import org.supercsv.prefs.CsvPreference;

import java.io.*;
import java.util.*;

/**
 * com.syntergy.utils
 * CsvUtils
 * Created by Kit on 3/23/2015.
 */
public class ValidationLogger {

    private CsvMapWriter mapWriter = null;
    private final File stackTraceFile;
    private final File logFile;
    private final File csvErrorFile;
    private int numErrors;

    private HashMap<Integer, String> lineErrorMap;
    private HashMap<Integer, Map<String, Object>> lineValueMap;

    private String[] csvHeader;

    private ICsvListWriter listWriter = null;
    private CellProcessor[] processors;
    private final String ERROR_COLUMN_NAME = "#ValidationErrors";

    public int getNumErrors() {
        return numErrors;
    }

    public ValidationLogger(String auditFolderPath) {

        this.lineErrorMap = new HashMap<Integer, String>();
        this.lineValueMap = new HashMap<Integer, Map<String, Object>>();
        this.numErrors = 0;

        this.csvErrorFile = new File(auditFolderPath + "validationResults.csv");
        this.stackTraceFile = new File(auditFolderPath + "stacktrace.log");
        this.logFile = new File(auditFolderPath + "debug.log");

    }

    public void logCollection(Collection<String> set) {

        try {
            for (String s : set) {
                SyntFileUtils.appendUTF8StringToFile(logFile, s + "\n", true);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }

    }

    public void logMessage(String msg) {

        try {
            SyntFileUtils.appendUTF8StringToFile(logFile, msg, true);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }

    }

    public void logException(Exception e) throws IOException {

        PrintWriter ps = new PrintWriter(stackTraceFile);
        e.printStackTrace(ps);
        ps.close();
    }

    public void close() throws IOException {
        if (listWriter != null) {
            listWriter.close();
        }
        if (mapWriter != null) {
            mapWriter.close();
        }

    }

    private void startFileMapWriter(String[] header) throws IOException {

        mapWriter = new CsvMapWriter(new FileWriter(csvErrorFile),
                CsvPreference.STANDARD_PREFERENCE);

        // get the header with validationResults added
        csvHeader = getCompleteHeader(header);

        //logMessage("header:\n");
        //logCollection(Arrays.asList(header));

        //logMessage("\ncompleteHeader:\n");
        //logCollection(Arrays.asList(completeHeader));

        // set the processors
        setProcessors();

        // write the header
        mapWriter.writeHeader(csvHeader);

    }

    public void testSetProcessor() {

        csvHeader = new String[]{"#ValidationError", "$FILENAME", "$OBJECTTYPE", "$TARGETPATH"};

        setProcessors();

        System.out.println(processors.length);

    }


    private void setProcessors() {
        processors = new CellProcessor[csvHeader.length];
        for (int i = 0; i < csvHeader.length; i++) {
            processors[i] = new Optional();
        }
    }

    private String[] getCompleteHeader(String[] header) {

        String[] newHeader;

        // add the #ValidationErrors column
        if (header[0].equals(ERROR_COLUMN_NAME)) {

            newHeader = header.clone();

        } else {
            newHeader = new String[header.length + 1];
            newHeader[0] = ERROR_COLUMN_NAME;

            int i = 1;
            for (String s : header) {
                newHeader[i] = s;
                i++;
            }
        }

        return newHeader;
    }

    public void registerCsvValues(Integer lineNo, Map<String, Object> csvLineData) {

        lineValueMap.put(lineNo, csvLineData);

    }

    public void addCsvError(String errMsg, Integer lineNo) {

        // add to the list of errors for this line number
        String errorStr = lineErrorMap.containsKey(lineNo) ? lineErrorMap.get(lineNo) + ";  " : "";
        errorStr += errMsg;
        lineErrorMap.put(lineNo, errorStr);
    }

    void writeCsvErrors(String[] rawFileHeader) throws IOException {

        startFileMapWriter(rawFileHeader);

        // using keySet() for iteration over keys
        for (Map.Entry<Integer, String> entry : lineErrorMap.entrySet()) {

            // increment number of errors
            numErrors += 1;

            Integer lineNo = entry.getKey();
            String errorStr = entry.getValue();

            Map<String, Object> csvData = lineValueMap.get(lineNo);

            //logMessage("csvData columns before adding error:\n");
            //logCollection(csvData.keySet());

            csvData.put(ERROR_COLUMN_NAME, errorStr);

            //logMessage("\ncsvData columns after adding error:\n");
            //logCollection(csvData.keySet());

            mapWriter.write(csvData, csvHeader, processors);
        }

    }

/*

    private void startFileListWriter(String[] header) throws IOException {

        listWriter = new CsvListWriter(new FileWriter(csvErrorFile),
                CsvPreference.STANDARD_PREFERENCE);

        // get the header with validationResults added
        String[] completeHeader = getCompleteHeader(header);

        // set the processors
        setProcessors(completeHeader);

        // write the header
        listWriter.writeHeader(completeHeader);

    }

      public void writeCsvError(String[] header, String errMsg, Integer lineNo, Collection<Object> csvLineData) throws IOException {

        // increment number of errors
        numErrors += 1;

        // add error message to 1st element in list
        ArrayList<Object> newList = new ArrayList<Object>(csvLineData.size() + 1);
        newList.add(0, errMsg);
        newList.addAll(1, csvLineData);

        // if it's the first row, we need to create a new writer and write out the header
        if (listWriter == null) {
            startFileListWriter(header);
        }

        listWriter.write(newList, processors);

        String lineMsg = lineErrorMap.get(lineNo);
        if (lineMsg == null) {
            lineErrorMap.put(lineNo, errMsg);
        } else {
            lineErrorMap.put(lineNo, lineMsg + "\n" + errMsg);
        }
    }
*/

    /*
    public void writeCsvError(String errMsg, Integer lineNo, Map<String, Object> csvLineData) throws IOException {

        // increment number of errors
        numErrors += 1;

        if (csvLineData!=null){
            // add error message to map
            csvLineData.put(ERROR_COLUMN_NAME, errMsg);

            // if it's the first row, we need to  create a new writer and write out the header
            if (mapWriter==null) {
                startFileMapWriter();
            }

            if (mapWriter!=null){
                mapWriter.write(csvLineData, header, processors);
            }

            if (lineErrorMap!=null){
                String lineMsg = lineErrorMap.get(lineNo);

                if (lineMsg == null) {
                    lineErrorMap.put(lineNo, errMsg);
                } else {
                    lineErrorMap.put(lineNo, lineMsg + "\n" + errMsg);
                }
            }
        }
    }
    */


    Map<Integer, String> getLineErrorMap() {

        return this.lineErrorMap;

        /*
        HashMap<Integer, String> stringMap = new HashMap<Integer, String>();


        for (Map.Entry<Integer, List<String>> entry : lineErrorMap.entrySet()) {
            // iterate through the strings
            ListIterator<String> msgIterator = entry.getValue().listIterator();
            StringBuilder errMsgs = new StringBuilder();
            while (msgIterator.hasNext()) {
                errMsgs.append(msgIterator.next()).append("\n");
            }

            stringMap.put(entry.getKey(), errMsgs.substring(0, errMsgs.lastIndexOf("\n") - 1));

        }
        */
    }

}
