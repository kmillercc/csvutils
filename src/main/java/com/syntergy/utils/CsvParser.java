package com.syntergy.utils;

import com.opentext.livelink.oml.OScriptObject;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvListReader;
import org.supercsv.io.CsvMapReader;
import org.supercsv.io.ICsvListReader;
import org.supercsv.io.ICsvMapReader;
import org.supercsv.prefs.CsvPreference;

import java.io.*;
import java.nio.charset.Charset;
import java.util.*;

public final class CsvParser {

    public static final String UTF8_BOM = "\uFEFF";
    private static int rowIndex;

    public static int getRowIndex() {
        return rowIndex;
    }

    public static Integer CheckCsvSize(String csvPath, int limit) throws ReturnedException, IOException {
        Integer numRows = 0;
        ICsvListReader listReader = null;
        try {
            listReader = new CsvListReader(new FileReader(csvPath), CsvPreference.STANDARD_PREFERENCE);

            String[] header = listReader.getHeader(true);
            final CellProcessor[] processors = CellProcessorMapper.getDefaultProcessors(header, null);

            while ((listReader.read(processors)) != null) {
                numRows += 1;
                if (numRows > limit) {
                    return numRows;
                }
            }
        } catch (Exception e) {
            throw new ReturnedException(e);

        } finally {
            if (listReader != null) {
                listReader.close();
            }
        }

        return numRows;
    }

    public static Map<String, Object> GetHeader(String csvPath) throws ReturnedException, IOException {
        return GetHeader(csvPath, true);
    }

    public static Map<String, Object> GetHeader(String csvPath, boolean removeBOM) throws ReturnedException, IOException {

        Map<String, Object> rtn = new HashMap<String, Object>();
        String[] header;
        ICsvMapReader mapReader = null;
        HeaderDataFlags headerDataFlags;

        try {
            mapReader = new CsvMapReader(new FileReader(csvPath), CsvPreference.STANDARD_PREFERENCE);

            // the header columns are used as the keys to the Map
            header = formatHeader(mapReader.getHeader(true), true, removeBOM);

            // data flags telling us what is in the CSV
            headerDataFlags = new HeaderDataFlags(header);

            // if target path is parent, then parse the first couple of lines to make sure
            if (headerDataFlags.get("targetPathIsParent")) {
                int sameCount = 0;
                final CellProcessor[] processors = CellProcessorMapper.getDefaultProcessors(header, null);
                Map<String, Object> csvLineData;
                int i = 0;
                while ((csvLineData = mapReader.read(header, processors)) != null && i < 5) {

                    String targetPath = (String) csvLineData.get("$TARGETPATH");
                    String objectName = (String) csvLineData.get("$OBJECTNAME");
                    int lastColon = targetPath.lastIndexOf(":");

                    if (lastColon > 0 && targetPath != null && objectName != null) {
                        String lastElement = targetPath.substring(lastColon + 1);
                        if (lastElement.equals(objectName)) {
                            sameCount++;
                        }
                    }

                    i++;
                }
                if (sameCount == i) {
                    headerDataFlags.put("targetPathIsParent", false);
                }
            }

        } catch (Exception e) {
            throw new ReturnedException(e);

        } finally {
            if (mapReader != null) {
                mapReader.close();
            }
        }

        // *** marshall the data and send back
        rtn.put("dataFlags", headerDataFlags);
        rtn.put("colNames", Arrays.asList(header));

        return rtn;
    }

    private static String removeUTF8BOM(String s) {
        if (s.startsWith(UTF8_BOM)) {
            char[] newChars = new char[s.length() - 1];
            s.getChars(1, s.length(), newChars, 0);
            String newStr = new String(newChars);
            return newStr;
        }
        return s;
    }

    private static String removeUTF8BOMWithLogging(String s) {
        ValidationLogger logger = new ValidationLogger("C:\\OPENTEXT\\CS105\\logs\\");
        //logger.logMessage("inside removeUTF8BOM\n");

        if (s.startsWith(UTF8_BOM)) {
            //logger.logMessage("Yes, string starts with BOM!\n");

            // characters before
            char[] chars = new char[s.length()];
            s.getChars(0, s.length(), chars, 0);
            logger.logMessage("\n\n\n");
            logger.logMessage("----------\n");
            logger.logMessage("BEFORE\n");
            for (int i = 0; i < chars.length; i++) {
                logger.logMessage("char at " + i + "=" + chars[i] + "\n");
            }

            // characters after
            char[] newChars = new char[s.length() - 1];
            s.getChars(1, s.length(), newChars, 0);
            logger.logMessage("\n");
            logger.logMessage("AFTER\n");
            for (int i = 0; i < newChars.length; i++) {
                logger.logMessage("char at " + i + "=" + newChars[i] + "\n");
            }

            String newStr = new String(newChars);
            return newStr;
        }

        try {
            logger.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return s;
    }

    @SuppressWarnings("unchecked")
    public static Map<String, Object> ParseDataIdFile(String filePath,
                                                      Integer startLineNo,
                                                      Integer endLineNo,
                                                      OScriptObject lineCallbackObj,
                                                      String lineCallbackFunc,
                                                      String auditPath) throws IOException {

        Map<String, Object> rtn = new HashMap<String, Object>();
        List<Map<String, Object>> data = new ArrayList<Map<String, Object>>();
        ICsvMapReader mapReader = null;
        String[] header = {"NodeID"};
        ValidationLogger logger = new ValidationLogger(auditPath);

        final CellProcessor[] processors = CellProcessorMapper.getDefaultProcessors(header, logger);
        try {
            mapReader = new CsvMapReader(new FileReader(filePath), CsvPreference.STANDARD_PREFERENCE);
            Map<String, Object> csvLineData;
            int i = 0;
            while ((csvLineData = mapReader.read(header, processors)) != null) {
                i++;
                // *** do we need to stop
                if (i > endLineNo) {
                    break;
                }
                // *** if we're in the right part of the CSV, call callback and add to data
                if (i >= startLineNo) {

                    if (lineCallbackObj != null) {
                        try {
                            csvLineData = (Map<String, Object>) OScriptInterface.invokeOScriptFunc(lineCallbackObj, lineCallbackFunc, i, csvLineData);
                            data.add(csvLineData);

                        } catch (OscriptException e) {
                            // 12-18-19 do not return an exception here. we want to continue parsing the file
                            // throw new ReturnedException(e, logger);

                            // log the error to a file because it's not easy to get info about these Java errors back to Oscript
                            try {
                                logger.logException(e);
                            } catch (Exception e2) {
                                e2.printStackTrace();
                            }
                        }
                    }
                }
            }
        } finally {
            logger.close();
            if (mapReader != null) {
                mapReader.close();
            }
        }

        rtn.put("data", data);
        return rtn;
    }


    public static Map<String, Object> ParseCsvFile(String csvPath,
                                                   Integer startLineNo,
                                                   Integer endLineNo,
                                                   OScriptObject lineCallbackObj,
                                                   String lineCallbackFunc) throws IOException, ReturnedException {

        Map<String, Object> rtn = new HashMap<String, Object>();

        ICsvMapReader mapReader = null;
        HeaderDataFlags headerDataFlags;
        List<Map<String, Object>> data = new ArrayList<Map<String, Object>>();
        String[] header;
        Reader reader = null;

        File file = new File(csvPath);
        String auditPath = file.getParent() + "/";
        ValidationLogger logger = new ValidationLogger(auditPath);

        try {
            InputStream stream = new FileInputStream(file);
            reader = new InputStreamReader(stream, Charset.forName("UTF-8"));
            mapReader = new CsvMapReader(reader, CsvPreference.STANDARD_PREFERENCE);

            // the header columns are used as the keys to the Map
            header = formatHeader(mapReader.getHeader(true), true);

            // data flags telling us what is in the CSV
            headerDataFlags = new HeaderDataFlags(header);

            final CellProcessor[] processors = CellProcessorMapper.getDefaultProcessors(header, logger);

            Map<String, Object> csvLineData;
            int i = 0;
            while ((csvLineData = mapReader.read(header, processors)) != null) {
                i++;

                // *** do we need to stop
                if (i > endLineNo) {
                    break;
                }

                // *** if we're in the right part of the CSV, call callback and add to data
                if (i >= startLineNo) {
                    if (lineCallbackObj != null) {
                        csvLineData = (Map<String, Object>) OScriptInterface.invokeOScriptFunc(lineCallbackObj, lineCallbackFunc, i, csvLineData);
                    }

                    data.add(csvLineData);
                }
            }
        } catch (Exception e) {

            throw new ReturnedException(e, logger);

        } finally {
            if (reader != null) {
                reader.close();
            }
            if (mapReader != null) {
                mapReader.close();
            }
        }

        // *** marshall the data and send back
        rtn.put("dataFlags", headerDataFlags);
        rtn.put("colNames", Arrays.asList(header));
        rtn.put("data", data);

        return rtn;
    }

    // note: since we are using what is, in effect, a global static variable rowIndex, we need to make this method synchronized
    // since this is only called from a single agent thread right now in Content Server, it is not an immediate cause for concern
    // However, this may change
    // TODO: revisit this later
    public static synchronized Map<String, Object> ValidateParseCsvFile(String csvPath,
                                                                        String auditPath,
                                                                        List<String> dateFormats,
                                                                        OScriptObject oScriptIterator,
                                                                        String csvLineCallback,
                                                                        String validateFuncName,
                                                                        Integer rowIndex) throws ReturnedException {


        CsvParser.rowIndex = rowIndex;
        Map<String, Object> rtn = new HashMap<String, Object>();
        ValidationLogger logger = new ValidationLogger(auditPath);

        try {
            doValidateParseCsvFile(csvPath, logger, dateFormats, oScriptIterator, csvLineCallback, validateFuncName, rowIndex);
        } catch (Exception e) {
            throw new ReturnedException(e, logger);
        }
        int numErrors = logger.getNumErrors();
        rtn.put("numErrors", numErrors);
        if (numErrors > 0) {
            rtn.put("lineErrorMap", logger.getLineErrorMap());
        }
        return rtn;
    }


    private static void doValidateParseCsvFile(String csvPath,
                                               ValidationLogger logger,
                                               List<String> dateFormats,
                                               OScriptObject oscriptIterator,
                                               String lineCallbackFunc,
                                               String validateFuncName,
                                               Integer rowIndex) throws IOException, OscriptException {

        ICsvMapReader mapReader;
        Reader reader;
        File file = new File(csvPath);
        InputStream stream = new FileInputStream(file);
        reader = new InputStreamReader(stream, Charset.forName("UTF-8"));

        mapReader = new CsvMapReader(reader, CsvPreference.STANDARD_PREFERENCE);

        // the header columns are used as the keys to the Map
        String[] header = formatHeader(mapReader.getHeader(true), false);

        // this is the map object that holds each lines data
        Map<String, Object> csvLineData;

        // call back to OScript to check column names
        OScriptInterface.invokeOScriptFunc(oscriptIterator, "CheckHeader", Arrays.asList(header));

        try {

            HeaderDataFlags headerDataFlags = new HeaderDataFlags(header);

            final CellProcessor[] processors = CellProcessorMapper.getProcessors(header, dateFormats, oscriptIterator, validateFuncName, logger);

            while ((csvLineData = mapReader.read(header, processors)) != null) {

                Map<String, Object> dataCopy = new HashMap<String, Object>(csvLineData);

                Integer lineNo = mapReader.getLineNumber() - 1;

                // put the values into the logger so that they can be logged out if there are errors
                logger.registerCsvValues(lineNo, csvLineData);

                // get data flags
                Map<String, Boolean> dataFlags = new LineDataFlags(dataCopy, headerDataFlags);

                if (oscriptIterator != null) {
                    try {
                        OScriptInterface.invokeOScriptFunc(oscriptIterator, lineCallbackFunc, rowIndex + lineNo, csvLineData, dataFlags);

                    } catch (OscriptException e) {

                        logger.addCsvError(e.getMessage(), lineNo);

                        if (e.isFatal()) {
                            throw e;
                        }
                    }
                }
            }

        } finally {
            if (logger != null) {
                logger.writeCsvErrors(header);
                logger.close();
            }
            if (reader != null) {
                reader.close();
            }
            if (mapReader != null) {
                mapReader.close();
            }
        }
    }

    static String[] formatHeader(String[] header, boolean replacePeriods) {
        return formatHeader(header, replacePeriods, true);
    }

    static String[] formatHeader(String[] header, boolean replacePeriods, boolean removeBOM) {

        ArrayList<String> newHeader = new ArrayList<String>();

        if (removeBOM && header.length > 0) {

            // String s = header[0];
            // characters before
            /* char[] chars = new char[s.length()];
            s.getChars(0, s.length(), chars, 0);
            System.out.println("BEFORE");
            for (int i = 0; i < chars.length-1; i++) {
                System.out.println(chars[i]);
            }*/

            header[0] = removeUTF8BOM(header[0]);

            /* s = header[0];
            // characters after
            char[] newChars = new char[s.length()];
            s.getChars(0, s.length(), newChars, 0);
            System.out.println("AFTER");
            for (int i = 0; i < newChars.length-1; i++) {
                System.out.println(newChars[i]);
            }*/

        }

        // convert header fields to upper case and trim them
        for (String col : header) {

            // also remove anything that starts with #
            //if (col.charAt(0) != '#') {

            if (col != null) {
                col = col.trim();

                // Leave categories and sys attrs alone because it messes things up
                String char1 = col.substring(0, 1);
                if (char1.equals("$")) {
                    col = col.toUpperCase().trim();
                }
                if (replacePeriods) {
                    // *** KM 12-17-15 escape any periods to that datatables does not complain
                    col = col.replaceAll("\\.", "@p");
                }

                newHeader.add(col);
            }

            //}

        }

        String[] arr = new String[newHeader.size()];
        return newHeader.toArray(arr);

    }

}
