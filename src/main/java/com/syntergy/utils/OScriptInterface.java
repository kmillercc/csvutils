package com.syntergy.utils;

import com.opentext.livelink.oml.OScriptObject;

import java.util.Map;

/**
 * Created by Kit on 3/18/2015.
 */
@SuppressWarnings("unchecked")
public class OScriptInterface {

    public static Object invokeOScriptFunc(OScriptObject o, String funcName, Object... args) throws OscriptException {

        Object rtn = null;

        if (o!=null) {
            Map<String, Object> status = (Map<String, Object>) o.invokeScript(funcName, args);
            Boolean ok = (Boolean) status.get("ok");
            if (!ok) {
                throw new OscriptException(status);
            }else{
                rtn = status.get("result");
            }
        }

        return rtn;
    }



}
