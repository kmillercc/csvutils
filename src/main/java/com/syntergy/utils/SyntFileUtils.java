//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.syntergy.utils;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;

class SyntFileUtils {


    public SyntFileUtils() {
    }


    private static FileOutputStream openOutputStream(File file, boolean append) throws IOException {
        if (file.exists()) {
            if (file.isDirectory()) {
                throw new IOException("File \'" + file + "\' exists but is a directory");
            }

            if (!file.canWrite()) {
                throw new IOException("File \'" + file + "\' cannot be written to");
            }
        } else {
            File parent = file.getParentFile();
            if (parent != null && !parent.mkdirs() && !parent.isDirectory()) {
                throw new IOException("Directory \'" + parent + "\' could not be created");
            }
        }

        return new FileOutputStream(file, append);
    }

    static OutputStream writeUTF8StringToFile(File file, String s, final boolean doClose) throws IOException, NoSuchMethodException {

        return writeStringToFile(file, s, Charset.forName("UTF-8"), false, doClose);

    }

    static OutputStream appendUTF8StringToFile(File file, String s, final boolean doClose) throws IOException, NoSuchMethodException {

        return appendStringToFile(file, s, Charset.forName("UTF-8"), doClose);

    }

    static void appendUTF8StringToFile(OutputStream out, String s, final boolean doClose) throws IOException, NoSuchMethodException {

         appendStringToOutputStream(out, s, Charset.forName("UTF-8"), doClose);

    }

    /**
     * Writes a String to a file creating the file if it does not exist.
     *
     * @param file     the file to write
     * @param data     the content to write to the file
     * @param encoding the encoding to use, {@code null} means platform default
     * @param append   if {@code true}, then the String will be added to the
     *                 end of the file rather than overwriting
     * @throws IOException in case of an I/O error
     * @since 2.3
     */
    private static OutputStream writeStringToFile(final File file, final String data, final Charset encoding, final boolean append, final boolean doClose) throws IOException, NoSuchMethodException {
        OutputStream out = null;
        try {
            out = openOutputStream(file, append);

            writeQuietly(out, data, encoding);

            if (doClose) {
                out.close();
            }
        } finally {
            if (doClose) {
                IOUtils.closeQuietly(out);
            }
        }

        return out;
    }

    /**
     * Append a String to a an existing file.
     *
     * @param file     the file to write
     * @param data     the content to write to the file
     * @param encoding the encoding to use, {@code null} means platform default
     * @param doClose  if {@code true}, then the File Output stream will be closed
     * @throws IOException in case of an I/O error
     * @since 2.3
     */
    private static OutputStream appendStringToFile(File file, final String data, final Charset encoding, final boolean doClose) throws IOException, NoSuchMethodException {
        OutputStream out = null;
        try {
            out = openOutputStream(file, true);

            writeQuietly(out, data, encoding);
            if (doClose) {
                out.close();
            }
        } finally {
            if (doClose) {
                IOUtils.closeQuietly(out);
            }
        }
        return out;
    }


    /**
     * Append a String to a an existing output stream.
     *
     * @param out      the file to write
     * @param data     the content to write to the file
     * @param encoding the encoding to use, {@code null} means platform default
     * @param doClose  if {@code true}, then the File Output stream will be closed
     * @throws IOException in case of an I/O error
     * @since 2.3
     */
    private static OutputStream appendStringToOutputStream(OutputStream out, final String data, final Charset encoding, final boolean doClose) throws IOException, NoSuchMethodException {
        try {
            writeQuietly(out, data, encoding);
            if (doClose) {
                out.close();
            }
        } finally {
            if (doClose) {
                IOUtils.closeQuietly(out);
            }
        }
        return out;
    }


    private static void writeQuietly(OutputStream out, final String data, final Charset encoding) throws IOException, NoSuchMethodException {
        // Apache IO 1.4 used by Brava won't have Charset class available
        // Use a try catch block to handle this possibility
        try {
            IOUtils.class.getMethod("write", String.class, OutputStream.class, Charset.class);
            IOUtils.write(data, out, encoding);
        } catch (NoSuchMethodException e) {
            try {
                IOUtils.class.getMethod("write", String.class, OutputStream.class, String.class);
                IOUtils.write(data, out, "UTF-8");
            } catch (NoSuchMethodException e2) {

                IOUtils.class.getMethod("write", String.class, OutputStream.class);
                IOUtils.write(data, out);

            }
        }
    }


}