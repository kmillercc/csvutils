package com.syntergy.utils;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.LineIterator;
import com.syntergy.json.CDL;
import com.syntergy.json.JSONArray;
import com.syntergy.json.JSONException;
import com.syntergy.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;

/**
 * Created by Kit on 3/17/2015.
 */
class CsvWriter {

    public static void JsonFileToCsv(String jsonPath,
                                     String csvPath,
                                     String firstRowJsonData,
                                     Integer sizeLimit) throws ReturnedException {

        try{
            File jsonFile = new File(jsonPath);
            long size = jsonFile.length();

            if (jsonFile.isFile() && size > (sizeLimit * 1000000)) {
                // System.out.println("using large file support");
                _LargeJsonFileToCsv(jsonFile, csvPath, firstRowJsonData);
            } else {
                // System.out.println("using regular file support");
                String jsonStr = "[" + firstRowJsonData + "," + readFileQuietly(jsonFile);
                JsonStrToCsv(jsonStr, csvPath);
            }
        }
        catch (Exception e){
            throw new ReturnedException(e);
        }

    }

    private static String readFileQuietly(File jsonFile) throws IOException, NoSuchMethodException {

        String jsonStr;

        // Apache IO 1.4 used by Brava won't have Charset class available
        // Use a try catch block to handle this possibility
        try {
            FileUtils.class.getMethod("readFileToString", File.class, Charset.class);
            jsonStr = FileUtils.readFileToString(jsonFile, Charset.forName("UTF-8"));

        } catch (NoSuchMethodException e) {
            try {
                FileUtils.class.getMethod("readFileToString", File.class, String.class);
                jsonStr = FileUtils.readFileToString(jsonFile, "UTF-8");
            } catch (NoSuchMethodException e2) {

                FileUtils.class.getMethod("readFileToString", File.class);
                jsonStr = FileUtils.readFileToString(jsonFile);

            }
        }

        return jsonStr;
    }

    private static void JsonStrToCsv(String jsonStr, String csvPath) throws Exception {

        File csvFile = new File(csvPath);

        JSONArray jsonArray = new JSONArray(jsonStr);
        String csvStr = CDL.toString(jsonArray);

        SyntFileUtils.writeUTF8StringToFile(csvFile, csvStr, true);

    }


    private static void _LargeJsonFileToCsv(File jsonFile,
                                            String csvPath,
                                            String firstRowJsonData) throws Exception {
        JSONArray names;
        OutputStream out;
        JSONObject jsonObject;
        String jsonStr;

        // new file
        File csvFile = new File(csvPath);

        // get the column names from first line
        // and write out the first line
        //jsonStr = formatForCsv(firstRowJsonData);
        jsonObject = new JSONObject(firstRowJsonData);
        names = jsonObject.names();
        jsonStr = "[" + firstRowJsonData + "]";
        out = startCsv(csvFile, jsonStr);

        // now iterate through the file, read in the json object on each line
        // convert it to a string and write it out to csv file
        // this is slow, but it allows us to support very large
        // exports that might otherwise cause the JVM to run out of heap space
        LineIterator it;
        try {
            FileUtils.class.getMethod("lineIterator", File.class, Charset.class);
            it = FileUtils.lineIterator(jsonFile, "UTF-8");
        } catch (NoSuchMethodException e) {
            it = FileUtils.lineIterator(jsonFile);
        }
        //int i=0;
        try {
            while (it.hasNext()) {
                //i++;
                jsonStr = it.nextLine();
                int len = jsonStr.length();
                // if the first or last column starts with a comma, get rid of it
                if (jsonStr.endsWith(",")) {
                    jsonStr = jsonStr.substring(0, len - 2);
                } else if (jsonStr.startsWith(",")) {
                    jsonStr = jsonStr.substring(1);
                }
                jsonStr = "[" + jsonStr + "]";
                appendToCsv(out, names, jsonStr);
            }
            if (out != null) {
                out.close();
            }
        } finally {
            LineIterator.closeQuietly(it);
            IOUtils.closeQuietly(out);
        }

    }


    private static OutputStream startCsv(File csvFile, String jsonStr) throws Exception {

        JSONArray jsonArray = new JSONArray(jsonStr);

        String csvStr = toString(jsonArray);

        return SyntFileUtils.writeUTF8StringToFile(csvFile, csvStr, false);

    }

    private static void appendToCsv(OutputStream out, JSONArray names, String jsonStr) throws Exception {

        JSONArray jsonArray = new JSONArray(jsonStr);

        String csvStr = toString(names, jsonArray);

        SyntFileUtils.appendUTF8StringToFile(out, csvStr, false);
    }


    /**
     * Produce a comma delimited text from a JSONArray of JSONObjects. The
     * first row will be a list of names obtained by inspecting the first
     * JSONObject.
     *
     * @param ja A JSONArray of JSONObjects.
     * @return A comma delimited text.
     * @throws JSONException
     */
    private static String toString(JSONArray ja) throws JSONException {
        JSONObject jo = ja.optJSONObject(0);
        if (jo != null) {
            JSONArray names = jo.names();
            if (names != null) {
                return rowToString(names) + toString(names, ja);
            }
        }
        return null;
    }


    /**
     * Produce a comma delimited text from a JSONArray of JSONObjects using
     * a provided list of names. The list of names is not included in the
     * output.
     *
     * @param names A JSONArray of strings.
     * @param ja    A JSONArray of JSONObjects.
     * @return A comma delimited text.
     * @throws JSONException
     */
    private static String toString(JSONArray names, JSONArray ja)
            throws JSONException {
        if (names == null || names.length() == 0) {
            return null;
        }
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < ja.length(); i += 1) {
            JSONObject jo = ja.optJSONObject(i);
            if (jo != null) {
                sb.append(rowToString(jo.toJSONArray(names)));
            }
        }
        return sb.toString();
    }

    /**
     * Produce a comma delimited text row from a JSONArray. Values containing
     * the comma character will be quoted. Troublesome characters may be
     * removed.
     *
     * @param ja A JSONArray of strings.
     * @return A string ending in NEWLINE.
     */
    private static String rowToString(JSONArray ja) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < ja.length(); i += 1) {
            if (i > 0) {
                sb.append(',');
            }
            Object object = ja.opt(i);
            if (object != null) {
                String string = object.toString();
                if (string.length() > 0 && (string.indexOf(',') >= 0 ||
                        string.indexOf('\n') >= 0 || string.indexOf('\r') >= 0 ||
                        string.indexOf(0) >= 0 || string.charAt(0) == '"') ||
                        string.contains("\"")) {
                    sb.append('"');
                    int length = string.length();
                    for (int j = 0; j < length; j += 1) {
                        char c = string.charAt(j);
                        if (c >= ' ' && c != '"') {
                            sb.append(c);
                        } else if (c == '"') {
                            sb.append("\"\"");
                        }
                    }
                    sb.append('"');
                } else {
                    sb.append(string);
                }
            }
        }
        sb.append('\n');
        return sb.toString();
    }

}
