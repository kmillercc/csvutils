package com.syntergy.utils.CellProcessors;

import com.opentext.livelink.oml.OScriptObject;
import com.syntergy.utils.CsvParser;
import com.syntergy.utils.OScriptInterface;
import com.syntergy.utils.ValidationLogger;
import org.supercsv.cellprocessor.CellProcessorAdaptor;
import org.supercsv.cellprocessor.ift.DateCellProcessor;
import org.supercsv.cellprocessor.ift.LongCellProcessor;
import org.supercsv.cellprocessor.ift.StringCellProcessor;
import org.supercsv.util.CsvContext;

import java.io.IOException;

/**
 * Created by Kit on 3/18/2015.
 */
public class ParseOScriptVal extends CellProcessorAdaptor implements DateCellProcessor, LongCellProcessor, StringCellProcessor {

    private final String colName;
    private final OScriptObject validateWrapper;
    private final String validateFuncName;
    private final ValidationLogger logger;

    /*
    public enum UTF8Columns {
        $OBJECTNAME, $TARGETPATH, $SOURCEPATH, $FILENAME, $NEWNAME, $NICKNAME
    }

    private static final Map<String, UTF8Columns> enumMap = EnumUtils.getEnumMap(UTF8Columns.class);
    */


    public ParseOScriptVal(OScriptObject validateWrapper,
                           Enum<?> colName, String validateFuncName, ValidationLogger logger) {
        super();
        this.colName = colName.toString();
        this.validateWrapper = validateWrapper;
        this.validateFuncName = validateFuncName;
        this.logger = logger;
    }

    public ParseOScriptVal(OScriptObject validateWrapper,
                           String colName, String validateFuncName, ValidationLogger logger) {
        super();
        this.colName = colName;
        this.validateWrapper = validateWrapper;
        this.validateFuncName = validateFuncName;
        this.logger = logger;
    }

    public Object execute(Object value, CsvContext context) {

        if (this.validateWrapper != null) {
            try {
                Integer rowIndex = CsvParser.getRowIndex() + context.getLineNumber() - 1;

                OScriptInterface.invokeOScriptFunc(validateWrapper, validateFuncName, rowIndex, colName, value);

            } catch (Exception e) {

                String msg = colName + ": " + e.getMessage();
                //msg += String.format(" at line %d, column %d.", context.getLineNumber() - 1, context.getColumnNumber());
                int lineNo = context.getLineNumber() -1;

                try {
                    logger.addCsvError(msg, lineNo);
                }
                catch(NullPointerException e2){
                    e2.printStackTrace();
                }

            }

        }

        return next.execute(value, context);
    }



}
