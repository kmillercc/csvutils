package com.syntergy.utils.CellProcessors;

import com.syntergy.utils.ValidationLogger;
import org.supercsv.cellprocessor.CellProcessorAdaptor;
import org.supercsv.cellprocessor.ift.DateCellProcessor;
import org.supercsv.exception.SuperCsvCellProcessorException;
import org.supercsv.util.CsvContext;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * com.syntergy.utils.CellProcessors
 * CsvUtils
 * Created by Kit on 3/23/2015.
 */
public class ParseOScriptDate extends CellProcessorAdaptor {

    /*
        overridden just to provide the name of the column in the error message
     */

    private final String colName;
    private final List<String> dateFormats;
    private final ValidationLogger logger;

    public ParseOScriptDate(Enum<?> col, List<String> dateFormats, DateCellProcessor next, ValidationLogger logger) {
        super(next);
        this.dateFormats = dateFormats;
        this.colName = col.toString();
        this.logger = logger;
    }

    @Override
    public Object execute(Object value, CsvContext context) {
        try {
            // *** allow "<clear>" to pass through
            String valStr = (String) value;
            valStr = valStr.trim();
            if (valStr.equalsIgnoreCase("<clear>") || valStr.equalsIgnoreCase("[clear]")){
                return next.execute(value, context);
            }
            value = tryParseDates(valStr, context);

        } catch (SuperCsvCellProcessorException e) {
            String msg = colName + " value " + e.getMessage().replaceFirst("could not be parsed as", "is expected to be");
            //msg += String.format(" at line %d, column %d.",context.getLineNumber()-1,context.getColumnNumber());
            int lineNo = context.getLineNumber() -1;

            try {
                logger.addCsvError(msg, lineNo);
            }
            catch(NullPointerException e2){
                e2.printStackTrace();
            }

        }
        return next.execute(value, context);
    }

    private Object tryParseDates(Object value, CsvContext context) throws SuperCsvCellProcessorException{

        SuperCsvCellProcessorException lastException = null;
        Integer numErrors = 0;

        for (String dateFormat : this.dateFormats) {
            try{
                value = tryParseDate(value, context, dateFormat);
            }
            catch  (SuperCsvCellProcessorException e){
                lastException = e;
                numErrors++;
            }
        }

        if (numErrors == this.dateFormats.size()){
            throw lastException;
        }
        else{
            return value;
        }
    }

    private Date tryParseDate(Object value, CsvContext context, String dateFormat) throws SuperCsvCellProcessorException{
        Date result;
        this.validateInputNotNull(value, context);
        if(!(value instanceof String)) {
            throw new SuperCsvCellProcessorException(String.class, value, context, this);
        } else {
            try {
                SimpleDateFormat e = new SimpleDateFormat(dateFormat);
                e.setLenient(true);
                result = e.parse((String)value);
            } catch (ParseException var5) {
                throw new SuperCsvCellProcessorException(String.format("\'%s\' could not be parsed as a Date", new Object[]{value}), context, this, var5);
            }
        }
        return result;
    }


}
