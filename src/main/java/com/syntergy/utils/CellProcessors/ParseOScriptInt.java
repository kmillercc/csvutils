package com.syntergy.utils.CellProcessors;

import com.syntergy.utils.ValidationLogger;
import org.supercsv.cellprocessor.ParseInt;
import org.supercsv.cellprocessor.ift.LongCellProcessor;
import org.supercsv.exception.SuperCsvCellProcessorException;
import org.supercsv.util.CsvContext;

import java.io.IOException;

/**
 * com.syntergy.utils.CellProcessors
 * CsvUtils
 * Created by Kit on 3/23/2015.
 */
public class ParseOScriptInt extends ParseInt {

    /*
        overridden just to provide the name of the column in the error message
     */

    private final String colName;
    private final ValidationLogger logger;

    public ParseOScriptInt(Enum<?> col, ValidationLogger logger) {
        super();
        this.colName = col.toString();
        this.logger = logger;
    }

    public ParseOScriptInt(Enum<?> col, LongCellProcessor next, ValidationLogger logger) {
        super(next);
        this.colName = col.toString();
        this.logger = logger;
    }

    @Override
    public Object execute(Object value, CsvContext context) {
        try {
            // *** allow "<clear>" to pass through
            String valStr = (String) value;
            valStr = valStr.trim();
            if (valStr.equalsIgnoreCase("<clear>") || valStr.equalsIgnoreCase("[clear]")){
                return next.execute(valStr, context);
            }
            return super.execute(valStr, context);
        } catch (SuperCsvCellProcessorException e) {
            String msg = colName + " value " + e.getMessage().replaceFirst("could not be parsed as", "is expected to be");
            //msg += String.format(" at line %d, column %d.",context.getLineNumber()-1,context.getColumnNumber());
            int lineNo = context.getLineNumber() -1;

            try {
                if (logger != null) {
                    logger.addCsvError(msg, lineNo);
                }
            }
            catch(NullPointerException e2){
                e2.printStackTrace();
            }

        }

        return next.execute(value, context);
    }
}
