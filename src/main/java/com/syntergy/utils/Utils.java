package com.syntergy.utils;

import com.syntergy.utils.ColumnHandlers.ColHandler;

import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;

/**
 * Created by Kit on 3/19/2015.
 */
class Utils {

    static <E extends Enum<E>> boolean isValidEnum(final Class<E> enumClass, final String key) {
        if (key == null) {
            return false;
        }
        try {
            Enum.valueOf(enumClass, key);
            return true;
        } catch (final IllegalArgumentException ex) {
            return false;
        }
    }

    static <E extends ColHandler> boolean arrayHasColumnType(final Class<E> mapClass, final List<String> keys) {

        try{
            Method m = mapClass.getMethod("contains", String.class);
            for (String k : keys) {
                if ((Boolean) m.invoke(null, k)){
                    return true;
                }
            }
            return false;
        }
        catch (Exception e){
            e.printStackTrace();
            return false;
        }

    }

    static <E extends ColHandler> boolean mapHasDataType(final Class<E> mapClass, final Map<String, Object> map, boolean removeKey) {
        try{
            Method m = mapClass.getMethod("getColumns");
            for (Enum<?> colEnum : (Enum<?>[]) m.invoke(null)) {
                String col = colEnum.toString();
                Object val = map.get(col);
                if (val != null && !val.equals("")){
                    if (removeKey){map.remove(col);}
                    return true;
                }
                if (removeKey){map.remove(col);}
            }
            return false;
        }
        catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    static <E extends Enum<E>> boolean mapHasColumn(final E e, final Map<String, Object> map, boolean removeKey) {
        String s = e.toString();
        Object val =map.get(s);
        if (val != null && !val.equals("")){
            if (removeKey){map.remove(s);}
            return true;
        }else{
            if (removeKey){map.remove(s);}
        }
        return false;
    }

    static boolean isSysAttrCol(String col){
        return !(col.substring(0,1).equals("$")) && !(col.contains(":"));
    }

    static boolean isCatAttrCol(String col){
        return !(col.substring(0,1).equals("$")) && col.contains(":");
    }


}
